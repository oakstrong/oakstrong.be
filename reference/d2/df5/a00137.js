var a00137 =
[
    [ "Format", "d2/df5/a00137.html#ae80dbe3672fdc2264fc1a01eeafaaeac", [
      [ "Png", "d2/df5/a00137.html#ae80dbe3672fdc2264fc1a01eeafaaeaca37ab10e6a47dbe77e6893051e7559a72", null ],
      [ "Bmp", "d2/df5/a00137.html#ae80dbe3672fdc2264fc1a01eeafaaeaca0ea4a8165f3dcb7f9940abf90dce9c69", null ],
      [ "Jpeg", "d2/df5/a00137.html#ae80dbe3672fdc2264fc1a01eeafaaeaca9111eb9d65aea4de0143890bd1e919f7", null ]
    ] ],
    [ "BitmapGraphicsPreferences", "d2/df5/a00137.html#ace7100296b56521b95ed6bb7497e65e8", null ],
    [ "Update", "d2/df5/a00137.html#a93c11c3812dd402436e5f1f303343888", null ],
    [ "m_format", "d2/df5/a00137.html#a84e45d0354cc0a27b7506b8394e98bda", null ],
    [ "m_size_preset", "d2/df5/a00137.html#a362bb86017a6be28407dfcdf9af39e89", null ],
    [ "m_size_x", "d2/df5/a00137.html#a49758d922ad2e537c6d31b78b6968ba5", null ],
    [ "m_size_y", "d2/df5/a00137.html#afbcfebe0eea9d72559c02d212793665c", null ]
];