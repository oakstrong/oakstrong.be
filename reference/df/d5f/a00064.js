var a00064 =
[
    [ "EditDataCommand", "df/d5f/a00064.html#a62bd4caee3c24b6c8623edfcb3b5697d", null ],
    [ "~EditDataCommand", "df/d5f/a00064.html#a9d9287ff44e4a82843f21f72a7df5d35", null ],
    [ "undo", "df/d5f/a00064.html#adbc6e0fd6ff815681b7b0e9219fff1b5", null ],
    [ "redo", "df/d5f/a00064.html#aaf26f20e1a7fd650a940a8efef215f79", null ],
    [ "model", "df/d5f/a00064.html#a32b58a4e1361d2ba6f2f1aa8d30fc489", null ],
    [ "item", "df/d5f/a00064.html#a77f0c9bb145ea0ebc96429e09c17d90e", null ],
    [ "index", "df/d5f/a00064.html#aa95f524a1fbfe52dc5af7c7ed37039d1", null ],
    [ "old_value", "df/d5f/a00064.html#ab4412da5733bc2eafbd15fa6edf91be8", null ],
    [ "new_value", "df/d5f/a00064.html#a70664ed5e34536471c1640a2beba9de3", null ]
];