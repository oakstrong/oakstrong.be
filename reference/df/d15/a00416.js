var a00416 =
[
    [ "Basic", "df/d15/a00416.html#a0bd47c7552dfe76fd6a6c35a743caa42", null ],
    [ "Basic", "df/d15/a00416.html#a07faf26fcd5c8813922aee481e493a9a", null ],
    [ "Initialize", "df/d15/a00416.html#a085de2dc1ac8a304c85b74926934909e", null ],
    [ "operator()", "df/d15/a00416.html#adbdc16a967b906e92260723fed3bdae0", null ],
    [ "PINflux", "df/d15/a00416.html#a78b10a8518853abcf50055d5a56f8e53", null ],
    [ "m_cd", "df/d15/a00416.html#a81780c3326b7b2dc35dfe76c58853264", null ],
    [ "m_k1", "df/d15/a00416.html#a1dd2520bd13d432d1eeace1206ebc495", null ],
    [ "m_k2", "df/d15/a00416.html#a93d811204282971118b9eba4d3b739a3", null ],
    [ "m_km", "df/d15/a00416.html#a1e0cc57422d02799fc91618b0fce466b", null ],
    [ "m_kr", "df/d15/a00416.html#a8363abaf35dd76820370041978bc207e", null ],
    [ "m_r", "df/d15/a00416.html#a160e946a199592849efea09c59b53444", null ]
];