var a00353 =
[
    [ "ModifiedGC", "db/ddc/a00353.html#a26a1cc97fb060736cae4198faf72de96", null ],
    [ "Initialize", "db/ddc/a00353.html#aef52bd4e88a7d5697cde127d46327ce1", null ],
    [ "operator()", "db/ddc/a00353.html#a9de5fe8e66be909175d932755058b447", null ],
    [ "m_cd", "db/ddc/a00353.html#a7f5e7c7115b483ecdc8bbc1890f55a9d", null ],
    [ "m_lambda_bend", "db/ddc/a00353.html#ab0e9476e141e52f009dc5550dccd221f", null ],
    [ "m_lambda_cell_length", "db/ddc/a00353.html#af0f598d8a51097b2c7d2735d73e799e0", null ],
    [ "m_lambda_length", "db/ddc/a00353.html#a9435cbb33cd29d1c519d6459fe77a528", null ],
    [ "m_rp_stiffness", "db/ddc/a00353.html#a6beaab204ce22299cc67ffcedf09b513", null ],
    [ "m_target_node_distance", "db/ddc/a00353.html#ab21f0eb30d1b7cde3fe1778bfd2a9dc6", null ]
];