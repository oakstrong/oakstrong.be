var a00151 =
[
    [ "ClientHandler", "dd/db2/a00151.html#a911181b2ad38bb2a20764e22d0cf9db3", null ],
    [ "DeleteExploration", "dd/db2/a00151.html#a9798b9353b6e50ffd044acae80425b69", null ],
    [ "DisplayError", "dd/db2/a00151.html#a2bee99b5dd010559aad311e9b186eedc", null ],
    [ "Refresh", "dd/db2/a00151.html#aed23f8ee6555cfdb6e5b0d0b41ea3644", null ],
    [ "RegisterExploration", "dd/db2/a00151.html#a076fe4f405b6b0e291318f7549777879", null ],
    [ "SendExplorationNames", "dd/db2/a00151.html#a0daed93ac8af7e7413b89af7f24da81d", null ],
    [ "Subscribe", "dd/db2/a00151.html#a03e19accb412a514360bf1f7b29c09b3", null ],
    [ "Unsubscribe", "dd/db2/a00151.html#a94898c26f3afaeb39c4fe57021200b5c", null ],
    [ "m_protocol", "dd/db2/a00151.html#aab3b964a23aff6dcba7155c643e7d864", null ],
    [ "m_exploration_manager", "dd/db2/a00151.html#a681dc23d205ff8a97717c63b7bd05638", null ],
    [ "m_subscribed_exploration", "dd/db2/a00151.html#a2ee349e29d142d2e3026063a99153746", null ]
];