var a00116 =
[
    [ "Workspace", "d4/d3e/a00116.html#ac0814f8b25559f5a3e8f116288201458", null ],
    [ "Workspace", "d4/d3e/a00116.html#adf58c5a5a493cafbbc2fb193e1a0c9b4", null ],
    [ "~Workspace", "d4/d3e/a00116.html#a788f537351a62ce1c25e364d6f88df24", null ],
    [ "Workspace", "d4/d3e/a00116.html#a3b3b0f2fc07828d91f20eafe56079c75", null ],
    [ "begin", "d4/d3e/a00116.html#a1d42a32d7634d27ec126a57750691fcd", null ],
    [ "begin", "d4/d3e/a00116.html#ae566ccfa6bedb3d8a7e9eea8fe45bed4", null ],
    [ "end", "d4/d3e/a00116.html#a0d35740f1719937f402215cb7705254c", null ],
    [ "end", "d4/d3e/a00116.html#a02a4e79c3e2b9f3201f4c3eeb827bea9", null ],
    [ "Add", "d4/d3e/a00116.html#a3dddb3a4e28c3276f812ee2ae1cc558d", null ],
    [ "Back", "d4/d3e/a00116.html#ac4b5375cbcc311399ac525df97fca830", null ],
    [ "Back", "d4/d3e/a00116.html#acf8a2b1d10a3b4362c8129c844f50fa1", null ],
    [ "Find", "d4/d3e/a00116.html#a77bbd504fd76034d052c0bce76dfe15b", null ],
    [ "Find", "d4/d3e/a00116.html#a815295ad33b775c9e7886e5d30529176", null ],
    [ "IsProject", "d4/d3e/a00116.html#a171de309e28a0f46a2550ce3267297b4", null ],
    [ "Front", "d4/d3e/a00116.html#ab12a90e4d493f717aae57de026d4ebb6", null ],
    [ "Front", "d4/d3e/a00116.html#a71dc9503c70dd0491cb27f6050b35510", null ],
    [ "Get", "d4/d3e/a00116.html#ae5037360c26ef175458806c3705aca45", null ],
    [ "Get", "d4/d3e/a00116.html#aa94e9cc976cda4dfd86c0d02dd765648", null ],
    [ "GetIndexFile", "d4/d3e/a00116.html#a25bd50fe751f49f0ac30b7217d2d2780", null ],
    [ "GetPath", "d4/d3e/a00116.html#a305337a965e5ae191dd8a8d3f41b9bbb", null ],
    [ "GetUserData", "d4/d3e/a00116.html#a6fe339b604a09c04ef9a7af33baefbc2", null ],
    [ "New", "d4/d3e/a00116.html#a54ba3d1729eb73840f230cfc964a06ef", null ],
    [ "Refresh", "d4/d3e/a00116.html#ab5be82c1f01857eed15306989216b30b", null ],
    [ "Rename", "d4/d3e/a00116.html#a875731c449b0508f2554e69ddb39248c", null ],
    [ "Rename", "d4/d3e/a00116.html#a08a7ff676542e49d720bbb1e926d9ee9", null ],
    [ "Remove", "d4/d3e/a00116.html#ae583ae1e8f3a3508b5c7736262eb49d1", null ],
    [ "Remove", "d4/d3e/a00116.html#ac27b016f70d6cb26455c1eb18db95a29", null ],
    [ "SetUserData", "d4/d3e/a00116.html#a82ea0836f3b9f755c7a49ae6dfc159a1", null ],
    [ "operator=", "d4/d3e/a00116.html#a4628197ab189a66a586aee970c6bcfdf", null ],
    [ "Store", "d4/d3e/a00116.html#a1adca191af026d2a40af9785b1b0a099", null ],
    [ "m_path", "d4/d3e/a00116.html#ac95decaf52d87b27acf7a3a4f5e670e0", null ],
    [ "m_prefs_file", "d4/d3e/a00116.html#a885daf818bc3c50d54229480bff15720", null ],
    [ "m_projects", "d4/d3e/a00116.html#ac462d14064270f214b000ed6f4179132", null ],
    [ "m_user_data", "d4/d3e/a00116.html#aa017b2dcafb551ac8c5dd1a35a878afb", null ],
    [ "m_filesystem_watcher", "d4/d3e/a00116.html#a182c3e3ac00cc5708faa4726c712ee10", null ]
];