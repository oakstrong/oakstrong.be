var a00069 =
[
    [ "RemoveRowsCommand", "d4/d31/a00069.html#ab059c31c3359fecc9514a8727d3b03ce", null ],
    [ "~RemoveRowsCommand", "d4/d31/a00069.html#a1c74a8a0a3a9dc7a319fdbeaae75b6b6", null ],
    [ "undo", "d4/d31/a00069.html#ae66868a9a6c6927bf95dfd7b9b1d5b58", null ],
    [ "redo", "d4/d31/a00069.html#a3605d8c50bca69359b66e62d47025a70", null ],
    [ "model", "d4/d31/a00069.html#a7a450267642f2553bc49419df015956a", null ],
    [ "parent", "d4/d31/a00069.html#a5b1187db62e3e25825f9a8d9edcbf862", null ],
    [ "parent_index", "d4/d31/a00069.html#a4e4b1242202d14e9045de3af49d08b5a", null ],
    [ "row", "d4/d31/a00069.html#a7754609fcd4baf15d1a45f84dd014d5b", null ],
    [ "count", "d4/d31/a00069.html#a03955c4dbf5b6a7866d3204288934cf1", null ],
    [ "undone", "d4/d31/a00069.html#ace6ee3b05ab7fa117a9f592ea784955d", null ],
    [ "items", "d4/d31/a00069.html#a028059fcb50b13896004edc0fc8421d6", null ]
];