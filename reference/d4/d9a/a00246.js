var a00246 =
[
    [ "UndoLeafStack", "d4/d9a/a00246.html#a9764791a61bb63583af55893d7d4e0f3", null ],
    [ "~UndoLeafStack", "d4/d9a/a00246.html#aad1c23e9ea9524c4127115906161a90c", null ],
    [ "Initialize", "d4/d9a/a00246.html#a39bce388c878f4b3713018891d345006", null ],
    [ "Push", "d4/d9a/a00246.html#a1b72be8ed5d948a25e978614cf03e1e3", null ],
    [ "CanUndo", "d4/d9a/a00246.html#a0d17b60c4c10a0ea5d5e204b38d47bc8", null ],
    [ "CanRedo", "d4/d9a/a00246.html#a49219e0048e6e214a395a8f6754abd36", null ],
    [ "Undo", "d4/d9a/a00246.html#a42951271aeebacf0d5ecb9fbdbd6bc2d", null ],
    [ "Redo", "d4/d9a/a00246.html#a007a38bfbe5cdab77c7bd576095812e8", null ],
    [ "m_stack", "d4/d9a/a00246.html#a45dfd4710b3ea3aa3ad9eb442944bd7b", null ],
    [ "m_current", "d4/d9a/a00246.html#a41059b6185b9f63038ffbe8e4f699b96", null ],
    [ "m_max_leafs", "d4/d9a/a00246.html#a571df680afbff7e2dc87da341ea1c38d", null ]
];