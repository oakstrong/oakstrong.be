var a00323 =
[
    [ "AreaThresholdBased", "de/d55/a00323.html#a8561ef17af998c8cfb016dd6b035c0ca", null ],
    [ "Initialize", "de/d55/a00323.html#af6c70c7d197e04d2b5454da03562e260", null ],
    [ "operator()", "de/d55/a00323.html#a9727992a189ad65681dfe677ad6a2526", null ],
    [ "m_base_area", "de/d55/a00323.html#a49e043efeef4ea502a6486dc6c086d02", null ],
    [ "m_cd", "de/d55/a00323.html#ac8034bb8ede5c530b1ff90e83e38f1a5", null ],
    [ "m_division_ratio", "de/d55/a00323.html#ae8d6f5f69da80b45bf481c5417a9f0bf", null ],
    [ "m_div_area", "de/d55/a00323.html#a7ce9f7e8be8ccd64099f79b86a96d027", null ]
];