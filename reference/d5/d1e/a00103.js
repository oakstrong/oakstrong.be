var a00103 =
[
    [ "Type", "d5/d1e/a00103.html#a5048d3f00711c728d5d56551ebcfaa76", [
      [ "ProjectAdded", "d5/d1e/a00103.html#a5048d3f00711c728d5d56551ebcfaa76a921b9abe3b1ef020a0db70cabd86cfee", null ],
      [ "ProjectRenamed", "d5/d1e/a00103.html#a5048d3f00711c728d5d56551ebcfaa76a3431a29c2185b0f505a6d86d23ea1ddf", null ],
      [ "ProjectRemoved", "d5/d1e/a00103.html#a5048d3f00711c728d5d56551ebcfaa76a1f22b2fe472d3c4f96ff605ac41dfa4b", null ]
    ] ],
    [ "WorkspaceChanged", "d5/d1e/a00103.html#a0808158324e3ad6e8581fb3c2241dcae", null ],
    [ "WorkspaceChanged", "d5/d1e/a00103.html#a1ec3634d1d805827b6fbb1b94dc2409c", null ],
    [ "~WorkspaceChanged", "d5/d1e/a00103.html#ac485725ae05178e6cc4b73de71cb9f94", null ],
    [ "GetType", "d5/d1e/a00103.html#a13b80fb501b4b033df37f26d2632f9d6", null ],
    [ "GetName", "d5/d1e/a00103.html#a985dfc72074f53735a5c1aa8d754dbee", null ],
    [ "GetOldName", "d5/d1e/a00103.html#a31551500a53649247381045611ab6080", null ],
    [ "GetNewName", "d5/d1e/a00103.html#a17ed00df8925d0e1b2d920c572938919", null ],
    [ "m_name", "d5/d1e/a00103.html#aa51d0d213b38eae72a11c0a241842ea1", null ],
    [ "m_old_name", "d5/d1e/a00103.html#a636e5cab158318bdd1490521ca367ab8", null ],
    [ "m_type", "d5/d1e/a00103.html#a5d08db7a3a6042d4e39f0cb50b591f6b", null ]
];