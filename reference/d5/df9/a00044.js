var a00044 =
[
    [ "ChildrenType", "d5/df9/a00044.html#aef3ce76fd718dad52c5591ed71da2d00", null ],
    [ "HasUnsavedChanges", "d5/df9/a00044.html#ac7e646a3ccc5a62a7acc0055bf1ab1d7", null ],
    [ "~HasUnsavedChanges", "d5/df9/a00044.html#a43402e3ab8fa628a8b8782beaef9c27e", null ],
    [ "begin", "d5/df9/a00044.html#aa5fc722335ba28d39b51ee4b7ba4c70e", null ],
    [ "begin", "d5/df9/a00044.html#a9643dd5e4e172a4c351f450e917d9a4a", null ],
    [ "end", "d5/df9/a00044.html#a4125b0bf4af9a0d3302ee99b1db14aef", null ],
    [ "end", "d5/df9/a00044.html#adbafd59854ef6a577145a41fe2275d24", null ],
    [ "ForceClose", "d5/df9/a00044.html#a091e25e1e741f736d60dc5eef1e852fd", null ],
    [ "GetTitle", "d5/df9/a00044.html#a5ff089eb3596bf2508b58207ce29cb5c", null ],
    [ "IsClean", "d5/df9/a00044.html#a4dc04cbaee0e4ace196590bce67797ab", null ],
    [ "IsOpened", "d5/df9/a00044.html#a80d543512f242a209d76559b5458bcab", null ],
    [ "Save", "d5/df9/a00044.html#ae4ff7b7846ac1c4476e3427dc9ed149f", null ],
    [ "SaveAndClose", "d5/df9/a00044.html#aa9ab1dc0fbba348664bdce78f419bb1f", null ],
    [ "AddChild", "d5/df9/a00044.html#a21fe99ef1aaf826b4221060a17918ca7", null ],
    [ "SetChildren", "d5/df9/a00044.html#a93e34d1291d1742404982a19c5969a39", null ],
    [ "InternalForceClose", "d5/df9/a00044.html#a15aa2cddf7d6e4f2022d6f3eadd4af8f", null ],
    [ "InternalIsClean", "d5/df9/a00044.html#a634d0552aeb99870fcf5b7ed8be1008c", null ],
    [ "InternalPreForceClose", "d5/df9/a00044.html#a7a16b8e588bd73533b8997bfef96ac5e", null ],
    [ "InternalSave", "d5/df9/a00044.html#a3bd25fa5852609521727c29d2a88b30d", null ],
    [ "m_title", "d5/df9/a00044.html#a9548362de2930401df1637d1d8dcfd02", null ],
    [ "m_children", "d5/df9/a00044.html#aa43389ada9b119fefe0b5e77ccfab594", null ]
];