var a00224 =
[
    [ "ServerInfo", "d5/ddf/a00224.html#a72f341fd1f8e910dfe49df7a4a6bb0b1", null ],
    [ "ServerInfo", "d5/ddf/a00224.html#aa67469920670a5c4d8670820deb1ec57", null ],
    [ "~ServerInfo", "d5/ddf/a00224.html#a3678e5b5a742400740b6fc3f19a3e2d1", null ],
    [ "Update", "d5/ddf/a00224.html#af52ec0375ec76fbafb02ae5773c92c41", null ],
    [ "GetName", "d5/ddf/a00224.html#aa555631dea9feb112ad4628f60ffa0e0", null ],
    [ "GetAddress", "d5/ddf/a00224.html#a6838dfc68e10b1980af06a94c671f879", null ],
    [ "GetPort", "d5/ddf/a00224.html#a7581a3f6b876cfb08427f4d481d9892a", null ],
    [ "m_name", "d5/ddf/a00224.html#a06df0527eea1b716acea2f8ba871f2e8", null ],
    [ "m_address", "d5/ddf/a00224.html#aed9e389e3c6f10942bdbc698006fbece", null ],
    [ "m_port", "d5/ddf/a00224.html#a4f43ee5b74029740ac851d0b34df260b", null ]
];