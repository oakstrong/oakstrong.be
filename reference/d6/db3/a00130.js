var a00130 =
[
    [ "EventType", "d6/db3/a00130.html#a84d22a8fdc9498e54040919d6a78c38b", null ],
    [ "KeyType", "d6/db3/a00130.html#ab8c510e3422f8ce16d4d6c9d7ee905d8", null ],
    [ "CallbackType", "d6/db3/a00130.html#a5788245dd41196153c8440fd23d0906a", null ],
    [ "Compare", "d6/db3/a00130.html#aa7ce5189a429ba0e173809a377ed9063", null ],
    [ "~Subject", "d6/db3/a00130.html#a05cef868da4c285ca014610fd80a6927", null ],
    [ "Register", "d6/db3/a00130.html#ab6e6828183f50ac6c3f6ac970322097a", null ],
    [ "Unregister", "d6/db3/a00130.html#a2a471f69d15ba42a05adf2817d4db36f", null ],
    [ "UnregisterAll", "d6/db3/a00130.html#a627a73fe0928c11f8728d59c37f2a1da", null ],
    [ "Notify", "d6/db3/a00130.html#a38740db084fda13bdc4a84741f6c6f10", null ],
    [ "m_observers", "d6/db3/a00130.html#a71f2abe50a972fbf3e2466a3492383da", null ]
];