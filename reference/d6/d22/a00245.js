var a00245 =
[
    [ "TriangularTile", "d6/d22/a00245.html#adda8e06d4bc25bbe3c2dc22ce56e3e79", null ],
    [ "~TriangularTile", "d6/d22/a00245.html#a4878d342467831fce515814548d36423", null ],
    [ "TriangularTile", "d6/d22/a00245.html#a62748cb5b3491bc91bbd94c1bf6623db", null ],
    [ "Left", "d6/d22/a00245.html#aece9037e2bfd943089c2501a321ba611", null ],
    [ "Right", "d6/d22/a00245.html#ac478f6f1e06e64daf17ff1e1a728c4c4", null ],
    [ "Up", "d6/d22/a00245.html#a14e18f75d08c6c7fda6e801fdba61a57", null ],
    [ "Down", "d6/d22/a00245.html#aa86e32cf2181be5b47631fcf98ff6f4a", null ],
    [ "m_odd", "d6/d22/a00245.html#afa26cdda23cafece9934f4fe1b11a663", null ],
    [ "g_side_length", "d6/d22/a00245.html#af4deba069d83470f643dd0de878f456d", null ],
    [ "g_height", "d6/d22/a00245.html#a431fe409765ae9eee1be34fd984d3179", null ],
    [ "g_start_polygon", "d6/d22/a00245.html#a05445439525132105a58e037e31bcbc1", null ]
];