var a00172 =
[
    [ "ExplorationManager", "d6/dfb/a00172.html#a1f884bb50eab75785de31eeb70cbf5bc", null ],
    [ "~ExplorationManager", "d6/dfb/a00172.html#a108d9b0a1a207c7a2a4c35a4ad006c1b", null ],
    [ "GetExplorationProgress", "d6/dfb/a00172.html#a167ca88d7f850511365c26559f8d591a", null ],
    [ "GetExplorationNames", "d6/dfb/a00172.html#a4b5c8a8ea1b6d93d0cc231b33845ebda", null ],
    [ "Updated", "d6/dfb/a00172.html#a12ff5096a00936b69c357dc989cb137f", null ],
    [ "RegisterExploration", "d6/dfb/a00172.html#a94fbf6c24747c77a47012e50ab538864", null ],
    [ "DeleteExploration", "d6/dfb/a00172.html#afbde50334641dc4240a718b4207a6df6", null ],
    [ "StopTask", "d6/dfb/a00172.html#a48e127896eee84fb341f290d6676105c", null ],
    [ "RestartTask", "d6/dfb/a00172.html#ad505829207543439ccf9d74540aa0f53", null ],
    [ "BackUp", "d6/dfb/a00172.html#a0d26346717a0f2db0c9e7ea230f94d98", null ],
    [ "NewWorkerAvailable", "d6/dfb/a00172.html#afe5c555d53ff4fda2bad9804ba7d5fb9", null ],
    [ "WorkerDisconnected", "d6/dfb/a00172.html#a4f75ca2752f3fcc3ffa4ba3ab4c0d41c", null ],
    [ "WorkerFinished", "d6/dfb/a00172.html#a19fde84d6c689c0c0049d521e7867c4e", null ],
    [ "WorkerReconnected", "d6/dfb/a00172.html#a1a8b1fa143ec8712acb62ba1fe4bc6d4", null ],
    [ "Read_Backup", "d6/dfb/a00172.html#a24ab697c55b50fb445bf01f828ed1ddd", null ],
    [ "WorkAvailable", "d6/dfb/a00172.html#a311bd83ccb54ad0d84b64b3336198f55", null ],
    [ "GetExploration", "d6/dfb/a00172.html#abb726247e7ae88524a5928b541d69745", null ],
    [ "RemoveExploration", "d6/dfb/a00172.html#a424732c8b2f606cb08ed324011d10af1", null ],
    [ "m_running_explorations", "d6/dfb/a00172.html#af63527149a164031fc725e296acc5ee8", null ],
    [ "m_done_explorations", "d6/dfb/a00172.html#a5f58b19125294adad06c5461a834a972", null ],
    [ "m_running_tasks", "d6/dfb/a00172.html#a04ed3d552fd271efc81425d4cd9b866c", null ]
];