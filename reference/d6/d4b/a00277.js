var a00277 =
[
    [ "BoundaryConditionCoupler", "d6/d4b/a00277.html#aeebfda31255a322c1300db82e0b0de61", null ],
    [ "~BoundaryConditionCoupler", "d6/d4b/a00277.html#a1339ccee0644664d2b3c1c2bd80c5c28", null ],
    [ "Initialize", "d6/d4b/a00277.html#ad52a07bb1202db777a923feae476ce63", null ],
    [ "Exec", "d6/d4b/a00277.html#a3398711e59dff8bdd4577b9f9ed01a1a", null ],
    [ "m_cd", "d6/d4b/a00277.html#a6f06ffe4175d4cc5bd3bd3a6cb807772", null ],
    [ "m_from", "d6/d4b/a00277.html#a5e230fe141b889d6287b01f043efde88", null ],
    [ "m_to", "d6/d4b/a00277.html#a719e4bd5a4c646d75e27f8640da4f135", null ],
    [ "m_transfer_chemicals", "d6/d4b/a00277.html#ae30d00ee3cd0856aff25fe8c2899922e", null ],
    [ "m_transfer_cells", "d6/d4b/a00277.html#acd1841ee8fafee0cce0cdc3890f59c30", null ]
];