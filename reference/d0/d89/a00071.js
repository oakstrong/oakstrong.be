var a00071 =
[
    [ "Result", "d0/d89/a00071.html#ade437a2561e9f2b035b8dd0e1c77b237", [
      [ "Save", "d0/d89/a00071.html#ade437a2561e9f2b035b8dd0e1c77b237a2ff8f238bfbfeb9addde0760b39da1b4", null ],
      [ "Cancel", "d0/d89/a00071.html#ade437a2561e9f2b035b8dd0e1c77b237ac153a4052cc1af23a218a3f6182e6bd2", null ],
      [ "Discard", "d0/d89/a00071.html#ade437a2561e9f2b035b8dd0e1c77b237a393f0b71e6978c9e40c182417530dc42", null ]
    ] ],
    [ "SaveChangesDialog", "d0/d89/a00071.html#a580aba6e7fc16af17ca1bdf768ee6d74", null ],
    [ "Prompt", "d0/d89/a00071.html#a132a2fe253763c6e0dd380ddbc33de77", null ],
    [ "SLOT_Clicked", "d0/d89/a00071.html#a1f5b34ff7bc8b16c8d1065cec9ad4034", null ],
    [ "m_save_button", "d0/d89/a00071.html#ad146b408371c2f5ce36c1271d513786e", null ],
    [ "m_cancel_button", "d0/d89/a00071.html#a97297d02e196b7563ce1718af2f37e93", null ],
    [ "m_discard_button", "d0/d89/a00071.html#a9a4d34b59f92ad3dab3eb64f90a48ab2", null ],
    [ "m_model", "d0/d89/a00071.html#a071d932d8198e59260686bde0e98c4c3", null ]
];