var a00210 =
[
    [ "Protocol", "da/de7/a00210.html#a8f6db45be5d6ccb4334d441dd537c464", null ],
    [ "Protocol", "da/de7/a00210.html#ad6a915deec74e02e1fd5498fdafb6d16", null ],
    [ "~Protocol", "da/de7/a00210.html#a20f976fdf12d63619b79949a3bc701f4", null ],
    [ "IsConnected", "da/de7/a00210.html#a1f1ac2caedf9f732ee2ef2d6c9c67d46", null ],
    [ "Ended", "da/de7/a00210.html#ace3855c0cf52094b5303bcdef2156ef1", null ],
    [ "Error", "da/de7/a00210.html#a98ba42f6d339d75874cc9f3251d2ef0d", null ],
    [ "SendPtree", "da/de7/a00210.html#a9cf5c8f8ff7a2e22432cbe119ff5f5ff", null ],
    [ "ReceivePtree", "da/de7/a00210.html#afb50880a07cdd30a5dc073de32d45f85", null ],
    [ "GetClientIP", "da/de7/a00210.html#abd9d4ae185a2189d36a12b9d1a88a8b9", null ],
    [ "GetClientPort", "da/de7/a00210.html#a2d89ea80da74b00862405a554b97daa4", null ],
    [ "Receive", "da/de7/a00210.html#ae90c62c2770890de989679e5c1be9eee", null ],
    [ "m_connection", "da/de7/a00210.html#a1da3f09141ba288ee7d6c026879748e5", null ]
];