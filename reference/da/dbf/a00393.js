var a00393 =
[
    [ "SmithPhyllotaxis", "da/dbf/a00393.html#af420197e5541efd1e5b070ad565dc5bb", null ],
    [ "Initialize", "da/dbf/a00393.html#ace0de0b2053a0611ba86d713ffa672bc", null ],
    [ "operator()", "da/dbf/a00393.html#ae2e068126969ea639874c5f956d53a87", null ],
    [ "m_cd", "da/dbf/a00393.html#a38994147deb7c41327e1e41c99d98649", null ],
    [ "m_D", "da/dbf/a00393.html#adbf59eedb3fb61cbb4090ab3b08363cb", null ],
    [ "m_T", "da/dbf/a00393.html#a028f82b4476eacc31fdf1240e63054f6", null ],
    [ "m_k_T", "da/dbf/a00393.html#ad4c87072917685d0f4fed0e0ed4ce6be", null ],
    [ "m_c", "da/dbf/a00393.html#ae9008febac93d8167aae2c784036f04d", null ]
];