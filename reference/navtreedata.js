var NAVTREE =
[
  [ "VLeaf2 Reference Manual", "index.html", [
    [ "VirtualLeaf2", "index.html", null ],
    [ "Namespaces", null, [
      [ "Namespace List", "namespaces.html", "namespaces" ],
      [ "Namespace Members", "namespacemembers.html", [
        [ "All", "namespacemembers.html", null ],
        [ "Functions", "namespacemembers_func.html", null ],
        [ "Variables", "namespacemembers_vars.html", null ],
        [ "Typedefs", "namespacemembers_type.html", null ],
        [ "Enumerations", "namespacemembers_enum.html", null ],
        [ "Enumerator", "namespacemembers_eval.html", null ]
      ] ]
    ] ],
    [ "Classes", null, [
      [ "Class List", "annotated.html", "annotated" ],
      [ "Class Index", "classes.html", null ],
      [ "Class Hierarchy", "hierarchy.html", "hierarchy" ],
      [ "Class Members", "functions.html", [
        [ "All", "functions.html", "functions_dup" ],
        [ "Functions", "functions_func.html", "functions_func" ],
        [ "Variables", "functions_vars.html", "functions_vars" ],
        [ "Typedefs", "functions_type.html", null ],
        [ "Enumerations", "functions_enum.html", null ],
        [ "Enumerator", "functions_eval.html", null ],
        [ "Related Functions", "functions_rela.html", null ]
      ] ]
    ] ],
    [ "Files", null, [
      [ "File List", "files.html", "files" ],
      [ "File Members", "globals.html", [
        [ "All", "globals.html", null ],
        [ "Functions", "globals_func.html", null ],
        [ "Macros", "globals_defs.html", null ]
      ] ]
    ] ]
  ] ]
];

var NAVTREEINDEX =
[
".html",
"d0/d81/a00180.html#ab2b811d9db4690a3ad2cf43d28a665c0",
"d1/d1e/a00087.html#a0b20ec4eb92ed5d49503719d7180540c",
"d1/da8/a00230.html#ab7eb3a26b431de927612cda921fda6c1",
"d2/d79/a00051.html#ad52b21ff244699085c1b893d64f9524a",
"d2/ddd/a00978_source.html",
"d3/db1/a00200.html#a3fad31c792c2ae08d6832c5a63efcd4a",
"d4/d4b/a00471_source.html",
"d4/ddf/a00488.html",
"d5/d89/a00907.html",
"d6/d42/a00043.html#a63bc9b945cc3287753f8c1dea3b482a8",
"d6/dfb/a00172.html#a1f884bb50eab75785de31eeb70cbf5bc",
"d7/d80/a00614_source.html",
"d8/d17/a00114.html#acafef390e54941e77bd255feb83da5ad",
"d8/df1/a00174.html#ab80a7161d47c3c1090eef555581c7583",
"d9/d73/a00060.html#a30988526f5dce1d66f04b4e23f5c7ccf",
"d9/dd0/a00191.html#a7efb9455e0036dee135816d36f690648",
"da/d75/a00334.html#aec0529db1b96b9f44e7d5d458292156e",
"db/d48/a00148.html#a8fc72c7dbd747bd0dae3eab51b8977c8",
"dc/d0f/a00303.html",
"dc/dc9/a00389.html#a117046826e6c7f2e386112692bfc2548",
"dd/d4e/a00329.html#a617dfd48d7d0c7874e79ba94e9c01f35",
"dd/de3/a00201.html#ab3d88472c633eecccd46211572ab03c3",
"de/d53/a00169.html#a0d28115610f5c2b599f3d2aecd7dac17",
"de/de6/a01063.html",
"df/d73/a00358.html#a99253d877bdc96858aac5c37db12e3d1",
"globals_defs.html"
];

var SYNCONMSG = 'click to disable panel synchronisation';
var SYNCOFFMSG = 'click to enable panel synchronisation';