var indexSectionsWithContent =
{
  0: "_abcdefghijklmnopqrstuvwxy~",
  1: "abcdefghilmnopqrstuvwx",
  2: "msuv",
  3: "abcdefghilmnopqrstuvwx",
  4: "abcdefghiklmnopqrstuvw~",
  5: "abcdefgiklmnoprstuvwxy",
  6: "bcdefhikmnprstvx",
  7: "bfimrst",
  8: "_abcdefijklmnprstw",
  9: "cmsuv",
  10: "qv",
  11: "v"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "typedefs",
  7: "enums",
  8: "enumvalues",
  9: "related",
  10: "defines",
  11: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Files",
  4: "Functions",
  5: "Variables",
  6: "Typedefs",
  7: "Enumerations",
  8: "Enumerator",
  9: "Friends",
  10: "Macros",
  11: "Pages"
};

