var searchData=
[
  ['name',['name',['../d1/d09/a00077.html#a7b38176bd512726cfb6ba42608298487',1,'SimShell::Gui::WorkspaceQtModel::Item::PR::name()'],['../db/df8/a00076.html#a3faf044f43a5e18997f2a7f08cd63339',1,'SimShell::Gui::WorkspaceQtModel::Item::FI::name()'],['../df/d98/a00107.html#a579ca0136a9ae27e7f89fd60a2b6d903',1,'SimShell::Ws::IProject::WidgetCallback::name()']]],
  ['navy',['navy',['../d5/d3e/a01139.html#a60fbcf07f7a28d43d38e4e46dc0fd1df',1,'VirtualLeaf::CellColor']]],
  ['nc',['nc',['../df/dc2/a00367.html#a4b7c886664da97726369e43a35e35f84',1,'VLeaf2_Sim::NCIncidence']]],
  ['new_5fparent',['new_parent',['../da/d24/a00068.html#a9268148878879e55fad21562dc24a2dd',1,'SimShell::Gui::PTreeModel::MoveRowsCommand']]],
  ['new_5fparent_5findex',['new_parent_index',['../da/d24/a00068.html#a3c7bb13bf46971197c8acfe71fb62981',1,'SimShell::Gui::PTreeModel::MoveRowsCommand']]],
  ['new_5frow',['new_row',['../da/d24/a00068.html#abbdd4437bced3e2488070c56c7aa4143',1,'SimShell::Gui::PTreeModel::MoveRowsCommand']]],
  ['new_5fvalue',['new_value',['../de/de1/a00065.html#a4cc9f3f675e8b0f1e03e80b42969ab3e',1,'SimShell::Gui::PTreeModel::EditKeyCommand::new_value()'],['../df/d5f/a00064.html#a70664ed5e34536471c1640a2beba9de3',1,'SimShell::Gui::PTreeModel::EditDataCommand::new_value()']]],
  ['nextboundaryp1',['nextBoundaryP1',['../d4/d04/a00255.html#a93f5ea2431e7257a780450d18ceb16f8',1,'VirtualLeaf::VoronoiTesselation::ClippedEdge']]],
  ['nextboundaryp2',['nextBoundaryP2',['../d4/d04/a00255.html#ad951645cf38ad55c815773e933f15f03',1,'VirtualLeaf::VoronoiTesselation::ClippedEdge']]]
];
