var a00347 =
[
    [ "GeoData", "d1/dc6/a00347.html#a067c93c212ff8ba0a09e40b53a5a67e1", null ],
    [ "GeoData", "d1/dc6/a00347.html#a20a6431768bb1fa08521c5dd4352b556", null ],
    [ "GetEllipseAxes", "d1/dc6/a00347.html#a45eb9f107bf85005b5265815c44768b2", null ],
    [ "operator+=", "d1/dc6/a00347.html#a09f0ee4ab50bc63c7c7a0f428a6b70a9", null ],
    [ "operator-=", "d1/dc6/a00347.html#a414ae00938746c17adab0c96933be402", null ],
    [ "m_area", "d1/dc6/a00347.html#af3dbb34c728736c4ff901c1cc84ca856", null ],
    [ "m_centroid", "d1/dc6/a00347.html#a307e543f5bc7bc02b7490b344402def4", null ],
    [ "m_area_moment", "d1/dc6/a00347.html#a4d71e3dd54463bff16b55c67f7774683", null ]
];