var a00087 =
[
    [ "GetTreeViewState", "d1/d1e/a00087.html#a2eabc197aeb4ac9a97e42a4cb4934706", null ],
    [ "SetTreeViewState", "d1/d1e/a00087.html#ad0cdc5c9aeb37e254f8f4d97e710ac6b", null ],
    [ "GetWidgetState", "d1/d1e/a00087.html#a6883fe7ef16916315b0f67fb1921a7f8", null ],
    [ "SetWidgetState", "d1/d1e/a00087.html#a65c4e5e80e220dec0216a36b8d247b32", null ],
    [ "GetDockWidgetState", "d1/d1e/a00087.html#a0b20ec4eb92ed5d49503719d7180540c", null ],
    [ "SetDockWidgetState", "d1/d1e/a00087.html#a7be63a342c8ccb5ed865cf688a706c57", null ],
    [ "GetDockWidgetArea", "d1/d1e/a00087.html#a5198042d02143bf73e8dacc44b5a4697", null ],
    [ "SetDockWidgetArea", "d1/d1e/a00087.html#ad80851de77054906f24015c78fce5cf0", null ],
    [ "g_dock_widget_area_to_string", "d1/d1e/a00087.html#ae7fba52b76e5df1a4f20cf3e6f6c37d7", null ],
    [ "g_string_to_dock_widget_area", "d1/d1e/a00087.html#a96e504ff00c688d5a9d37834a0e46ec8", null ]
];