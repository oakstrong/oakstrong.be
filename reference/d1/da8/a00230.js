var a00230 =
[
    [ "ResultType", "d1/da8/a00230.html#acc1f74795ebd05577994d1d2aa800dc7", [
      [ "Success", "d1/da8/a00230.html#acc1f74795ebd05577994d1d2aa800dc7a505a83f220c02df2f85c3810cd9ceb38", null ],
      [ "Failure", "d1/da8/a00230.html#acc1f74795ebd05577994d1d2aa800dc7ae139a585510a502bbf1841cf589f5086", null ],
      [ "Stopped", "d1/da8/a00230.html#acc1f74795ebd05577994d1d2aa800dc7ac23e2b09ebe6bf4cb5e2a9abe85c0be2", null ]
    ] ],
    [ "SimResult", "d1/da8/a00230.html#ae28749b9ecab4386537e8d64c188b815", null ],
    [ "SimResult", "d1/da8/a00230.html#a4ffa2431cbba6b4800b6f6fd2184018b", null ],
    [ "GetExplorationName", "d1/da8/a00230.html#ac286d2ad05394fd7ca5f63439262e336", null ],
    [ "GetTaskId", "d1/da8/a00230.html#a6f7c260feff6394b64a6f582bd6b35b2", null ],
    [ "GetResult", "d1/da8/a00230.html#ab7eb3a26b431de927612cda921fda6c1", null ],
    [ "GetNodeIP", "d1/da8/a00230.html#a50c2ee5cb0fba21c55511773d0157fa6", null ],
    [ "GetNodePort", "d1/da8/a00230.html#a6a5e64df3bfdbd3de23a1066bb3242c9", null ],
    [ "m_exploration_name", "d1/da8/a00230.html#af083da9b7b0ed0e79e81492899180dff", null ],
    [ "m_task_id", "d1/da8/a00230.html#a08f7503949c4567f954edf1fe54afd9f", null ],
    [ "m_success", "d1/da8/a00230.html#a5810f3afaa59aa5201dabb2a9101a3bb", null ],
    [ "m_node_ip", "d1/da8/a00230.html#aeb7e3ecb0be27c04a2da93c7a1beffbd", null ],
    [ "m_node_port", "d1/da8/a00230.html#a48d3af993079315af80b5fd3559dcc26", null ]
];