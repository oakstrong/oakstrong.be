var files =
[
    [ "ApoplastItem.cpp", "db/d43/a00422.html", null ],
    [ "ApoplastItem.h", "d1/d05/a00423.html", [
      [ "ApoplastItem", "d5/deb/a00132.html", "d5/deb/a00132" ]
    ] ],
    [ "AppController.cpp", "dc/df3/a00424.html", null ],
    [ "AppController.h", "dc/d2f/a00425.html", [
      [ "AppController", "d3/de7/a00039.html", "d3/de7/a00039" ]
    ] ],
    [ "AreaMoment.h", "d7/db9/a00426.html", "d7/db9/a00426" ],
    [ "AreaThresholdBased.cpp", "da/db6/a00427.html", null ],
    [ "AreaThresholdBased.h", "d4/dd3/a00428.html", [
      [ "AreaThresholdBased", "de/d55/a00323.html", "de/d55/a00323" ]
    ] ],
    [ "array3.h", "d4/d53/a00429.html", "d4/d53/a00429" ],
    [ "ArrowItem.cpp", "dc/d0c/a00430.html", null ],
    [ "ArrowItem.h", "df/d70/a00431.html", [
      [ "ArrowItem", "d0/de8/a00133.html", "d0/de8/a00133" ]
    ] ],
    [ "AttributeContainer.cpp", "d9/dd5/a00432.html", null ],
    [ "AttributeContainer.h", "dd/d07/a00433.html", "dd/d07/a00433" ],
    [ "cell_daughters/Auxin.cpp", "d1/d7e/a00434.html", null ],
    [ "cell_housekeep/Auxin.cpp", "d0/dc5/a00435.html", null ],
    [ "cell_daughters/Auxin.h", "d4/d82/a00436.html", [
      [ "Auxin", "d9/d5e/a00295.html", "d9/d5e/a00295" ]
    ] ],
    [ "cell_housekeep/Auxin.h", "d4/da8/a00437.html", [
      [ "Auxin", "d5/d0f/a00308.html", "d5/d0f/a00308" ]
    ] ],
    [ "cell_chemistry/AuxinGrowth.cpp", "dd/dfd/a00438.html", null ],
    [ "cell_housekeep/AuxinGrowth.cpp", "d9/d3a/a00439.html", null ],
    [ "cell_split/AuxinGrowth.cpp", "de/df8/a00440.html", null ],
    [ "reaction_transport/AuxinGrowth.cpp", "d6/d66/a00441.html", null ],
    [ "wall_chemistry/AuxinGrowth.cpp", "db/d34/a00442.html", null ],
    [ "cell_chemistry/AuxinGrowth.h", "d0/dc4/a00443.html", [
      [ "AuxinGrowth", "d2/d04/a00281.html", "d2/d04/a00281" ]
    ] ],
    [ "cell_housekeep/AuxinGrowth.h", "d3/dea/a00444.html", [
      [ "AuxinGrowth", "d2/d71/a00309.html", "d2/d71/a00309" ]
    ] ],
    [ "cell_split/AuxinGrowth.h", "dd/d47/a00445.html", [
      [ "AuxinGrowth", "d4/d8f/a00324.html", "d4/d8f/a00324" ]
    ] ],
    [ "reaction_transport/AuxinGrowth.h", "d3/def/a00446.html", [
      [ "AuxinGrowth", "d2/d56/a00384.html", "d2/d56/a00384" ]
    ] ],
    [ "wall_chemistry/AuxinGrowth.h", "de/d84/a00447.html", [
      [ "AuxinGrowth", "d0/dd6/a00415.html", "d0/dd6/a00415" ]
    ] ],
    [ "AuxinPIN1.cpp", "de/dfe/a00448.html", null ],
    [ "AuxinPIN1.h", "d9/d10/a00449.html", [
      [ "AuxinPIN1", "d4/ddd/a00138.html", "d4/ddd/a00138" ]
    ] ],
    [ "BackgroundDialog.cpp", "de/df4/a00450.html", null ],
    [ "BackgroundDialog.h", "d9/d69/a00451.html", [
      [ "BackgroundDialog", "de/d76/a00134.html", "de/d76/a00134" ]
    ] ],
    [ "reaction_transport/Basic.cpp", "d1/d0b/a00452.html", null ],
    [ "wall_chemistry/Basic.cpp", "da/dc3/a00453.html", null ],
    [ "reaction_transport/Basic.h", "d7/d9f/a00454.html", [
      [ "Basic", "d5/d05/a00385.html", "d5/d05/a00385" ]
    ] ],
    [ "wall_chemistry/Basic.h", "dd/d63/a00455.html", [
      [ "Basic", "df/d15/a00416.html", "df/d15/a00416" ]
    ] ],
    [ "BasicAuxin.cpp", "de/d87/a00456.html", null ],
    [ "BasicAuxin.h", "d1/dbd/a00457.html", [
      [ "BasicAuxin", "d1/d67/a00310.html", "d1/d67/a00310" ]
    ] ],
    [ "BasicPIN.cpp", "da/d65/a00458.html", null ],
    [ "BasicPIN.h", "d2/d91/a00459.html", [
      [ "BasicPIN", "db/d7b/a00296.html", "db/d7b/a00296" ]
    ] ],
    [ "BitmapGraphicsExporter.cpp", "db/db1/a00460.html", null ],
    [ "BitmapGraphicsExporter.h", "d5/d5f/a00461.html", [
      [ "BitmapGraphicsExporter", "d0/d5e/a00136.html", "d0/d5e/a00136" ]
    ] ],
    [ "BitmapGraphicsPreferences.h", "de/dcb/a00462.html", [
      [ "BitmapGraphicsPreferences", "d2/df5/a00137.html", "d2/df5/a00137" ]
    ] ],
    [ "BladCC.cpp", "d3/d5d/a00463.html", null ],
    [ "BladCC.h", "d2/d9d/a00464.html", [
      [ "BladCC", "dc/dc9/a00282.html", "dc/dc9/a00282" ]
    ] ],
    [ "BladCCCoupled.cpp", "db/de0/a00465.html", null ],
    [ "BladCCCoupled.h", "db/d2c/a00466.html", [
      [ "BladCCCoupled", "dd/dfe/a00283.html", "dd/dfe/a00283" ]
    ] ],
    [ "BladCD.cpp", "d7/d5e/a00467.html", null ],
    [ "BladCD.h", "df/d38/a00468.html", [
      [ "BladCD", "d3/d4a/a00297.html", "d3/d4a/a00297" ]
    ] ],
    [ "BladCH.cpp", "dc/ddf/a00469.html", null ],
    [ "BladCH.h", "d0/d29/a00470.html", [
      [ "BladCH", "d1/d5d/a00311.html", "d1/d5d/a00311" ]
    ] ],
    [ "BladColor.cpp", "d4/d4b/a00471.html", null ],
    [ "BladColor.h", "d7/db5/a00472.html", [
      [ "BladColor", "de/d14/a00139.html", "de/d14/a00139" ]
    ] ],
    [ "BladCS.cpp", "dc/d8e/a00473.html", null ],
    [ "BladCS.h", "d5/d26/a00474.html", [
      [ "BladCS", "de/dc8/a00325.html", "de/dc8/a00325" ]
    ] ],
    [ "BladRT.cpp", "d1/d92/a00475.html", null ],
    [ "BladRT.h", "d9/d34/a00476.html", [
      [ "BladRT", "dd/df5/a00386.html", "dd/df5/a00386" ]
    ] ],
    [ "BoundaryConditionCoupler.cpp", "d9/dca/a00477.html", null ],
    [ "BoundaryConditionCoupler.h", "d9/dea/a00478.html", [
      [ "BoundaryConditionCoupler", "d6/d4b/a00277.html", "d6/d4b/a00277" ]
    ] ],
    [ "BoundaryType.h", "d9/d4a/a00479.html", "d9/d4a/a00479" ],
    [ "CBMBuilder.cpp", "d9/dc5/a00480.html", null ],
    [ "CBMBuilder.h", "d3/d58/a00481.html", [
      [ "CBMBuilder", "dc/d5d/a00278.html", "dc/d5d/a00278" ]
    ] ],
    [ "Cell.cpp", "db/d8e/a00482.html", "db/d8e/a00482" ],
    [ "Cell.h", "dc/da0/a00483.html", "dc/da0/a00483" ],
    [ "CellAttributes.cpp", "d8/d89/a00484.html", "d8/d89/a00484" ],
    [ "CellAttributes.h", "d6/d1a/a00485.html", "d6/d1a/a00485" ],
    [ "CellDivider.cpp", "db/df1/a00486.html", null ],
    [ "CellDivider.h", "dd/d9c/a00487.html", [
      [ "CellDivider", "d0/da2/a00307.html", "d0/da2/a00307" ]
    ] ],
    [ "CellHousekeeper.cpp", "d4/ddf/a00488.html", null ],
    [ "CellHousekeeper.h", "da/d05/a00489.html", [
      [ "CellHousekeeper", "de/de9/a00322.html", "de/de9/a00322" ]
    ] ],
    [ "CellType.h", "d7/daa/a00490.html", "d7/daa/a00490" ],
    [ "ChainHull.cpp", "d6/d60/a00491.html", "d6/d60/a00491" ],
    [ "ChainHull.h", "da/d7e/a00492.html", "da/d7e/a00492" ],
    [ "CheckableTreeModel.cpp", "d9/da5/a00493.html", null ],
    [ "CheckableTreeModel.h", "d5/d0f/a00494.html", [
      [ "CheckableTreeModel", "d1/dc6/a00037.html", "d1/dc6/a00037" ],
      [ "Item", "d8/d41/a00038.html", "d8/d41/a00038" ]
    ] ],
    [ "ChemBlue.cpp", "db/dc4/a00495.html", null ],
    [ "ChemBlue.h", "d3/dad/a00496.html", [
      [ "ChemBlue", "d5/db1/a00140.html", "d5/db1/a00140" ]
    ] ],
    [ "ChemGreen.cpp", "d4/d1e/a00497.html", null ],
    [ "ChemGreen.h", "d8/d57/a00498.html", [
      [ "ChemGreen", "d5/d4e/a00141.html", "d5/d4e/a00141" ]
    ] ],
    [ "circular_iterator.h", "d8/d67/a00499.html", null ],
    [ "CircularIterator.h", "de/d4e/a00500.html", "de/d4e/a00500" ],
    [ "CircularIteratorImpl.h", "d8/db6/a00501.html", [
      [ "CircularIterator", "df/d6a/a00119.html", "df/d6a/a00119" ]
    ] ],
    [ "CliController.cpp", "d0/da2/a00502.html", null ],
    [ "CliController.h", "d4/de5/a00503.html", [
      [ "CliController", "db/d48/a00148.html", "db/d48/a00148" ]
    ] ],
    [ "CliConverterTask.h", "dc/d1c/a00504.html", "dc/d1c/a00504" ],
    [ "Client.cpp", "d7/d37/a00505.html", null ],
    [ "Client.h", "d4/d1f/a00506.html", [
      [ "Client", "d9/d7d/a00150.html", "d9/d7d/a00150" ]
    ] ],
    [ "ClientHandler.cpp", "d1/d69/a00507.html", null ],
    [ "ClientHandler.h", "dc/d30/a00508.html", [
      [ "ClientHandler", "dd/db2/a00151.html", "dd/db2/a00151" ]
    ] ],
    [ "ClientProtocol.cpp", "df/db6/a00509.html", null ],
    [ "ClientProtocol.h", "d6/d52/a00510.html", [
      [ "ClientProtocol", "d7/d9a/a00152.html", "d7/d9a/a00152" ]
    ] ],
    [ "CliTask.h", "d6/d10/a00511.html", "d6/d10/a00511" ],
    [ "CliWorkspaceManager.cpp", "d4/dfb/a00512.html", null ],
    [ "CliWorkspaceManager.h", "da/def/a00513.html", [
      [ "CliWorkspaceManager", "d3/d65/a00154.html", "d3/d65/a00154" ]
    ] ],
    [ "ClockCLib.h", "d6/d90/a00514.html", [
      [ "ClockCLib", "d8/dfa/a00121.html", "d8/dfa/a00121" ]
    ] ],
    [ "ClockTraits.h", "dc/d19/a00515.html", [
      [ "ClockTraits", "d8/d84/a00001.html", "d8/d84/a00001" ]
    ] ],
    [ "ColorizerMap.cpp", "d4/d40/a00516.html", null ],
    [ "ColorizerMap.h", "d8/dc7/a00517.html", [
      [ "ColorizerMap", "d2/d82/a00142.html", "d2/d82/a00142" ]
    ] ],
    [ "Compressor.cpp", "d5/dee/a00518.html", null ],
    [ "Compressor.h", "dd/d4d/a00519.html", [
      [ "Compressor", "dd/dd7/a00260.html", "dd/dd7/a00260" ]
    ] ],
    [ "Connection.cpp", "d2/da3/a00520.html", "d2/da3/a00520" ],
    [ "Connection.h", "d7/def/a00521.html", [
      [ "Connection", "d3/df8/a00155.html", "d3/df8/a00155" ]
    ] ],
    [ "constants.h", "d8/d14/a00522.html", "d8/d14/a00522" ],
    [ "ConstCircularIterator.h", "d0/de7/a00523.html", "d0/de7/a00523" ],
    [ "ConversionList.cpp", "d9/d24/a00524.html", null ],
    [ "ConversionList.h", "d7/db7/a00525.html", [
      [ "ConversionList", "d2/d34/a00156.html", "d2/d34/a00156" ],
      [ "EntryType", "d1/db7/a00157.html", "d1/db7/a00157" ]
    ] ],
    [ "converter_cli.cpp", "d0/d35/a00526.html", null ],
    [ "converter_gui.cpp", "d9/d44/a00527.html", null ],
    [ "ConverterWindow.cpp", "d5/d81/a00528.html", null ],
    [ "ConverterWindow.h", "d1/d0b/a00529.html", [
      [ "ConverterWindow", "d6/db8/a00158.html", "d6/db8/a00158" ]
    ] ],
    [ "CopyAttributesDialog.cpp", "da/d7e/a00530.html", null ],
    [ "CopyAttributesDialog.h", "d0/d2a/a00531.html", [
      [ "CopyAttributesDialog", "de/db9/a00159.html", "de/db9/a00159" ]
    ] ],
    [ "CoreData.h", "d7/d41/a00532.html", [
      [ "CoreData", "df/d91/a00335.html", "df/d91/a00335" ]
    ] ],
    [ "CoupledCliController.cpp", "db/d44/a00533.html", null ],
    [ "CoupledCliController.h", "dc/d20/a00534.html", [
      [ "CoupledCliController", "de/d38/a00160.html", "de/d38/a00160" ]
    ] ],
    [ "CoupledSim.cpp", "d7/d84/a00535.html", null ],
    [ "CoupledSim.h", "d4/d33/a00536.html", [
      [ "CoupledSim", "dd/d1d/a00336.html", "dd/d1d/a00336" ]
    ] ],
    [ "CoupledSimEvent.h", "d8/d6e/a00537.html", [
      [ "CoupledSimEvent", "d9/d48/a00345.html", "d9/d48/a00345" ]
    ] ],
    [ "CouplerFactories.cpp", "dd/d5a/a00538.html", null ],
    [ "CouplerFactories.h", "d3/d87/a00539.html", [
      [ "CouplerFactories", "d4/dce/a00337.html", "d4/dce/a00337" ]
    ] ],
    [ "CsvExporter.cpp", "d4/d79/a00540.html", null ],
    [ "CsvExporter.h", "dd/d72/a00541.html", [
      [ "CsvExporter", "d6/d7d/a00161.html", "d6/d7d/a00161" ]
    ] ],
    [ "CsvGzExporter.cpp", "d0/ddb/a00542.html", null ],
    [ "CsvGzExporter.h", "d4/d9c/a00543.html", [
      [ "CsvGzExporter", "dd/dae/a00163.html", "dd/dae/a00163" ]
    ] ],
    [ "CumulativeRecords.h", "d2/ddf/a00544.html", "d2/ddf/a00544" ],
    [ "DHelper.h", "d8/deb/a00545.html", [
      [ "DHelper", "dc/d84/a00338.html", "dc/d84/a00338" ]
    ] ],
    [ "DiamondTile.cpp", "dc/d2b/a00546.html", null ],
    [ "DiamondTile.h", "d9/d8b/a00547.html", [
      [ "DiamondTile", "d0/d48/a00165.html", "d0/d48/a00165" ]
    ] ],
    [ "DirectedNormal.h", "dd/de6/a00548.html", [
      [ "DirectedNormal", "de/dd7/a00362.html", "de/dd7/a00362" ]
    ] ],
    [ "DirectedUniform.h", "d5/d21/a00549.html", [
      [ "DirectedUniform", "d9/d9b/a00363.html", "d9/d9b/a00363" ]
    ] ],
    [ "directories.doxy", "de/de7/a00550.html", null ],
    [ "dummy.cpp", "d9/d87/a00551.html", "d9/d87/a00551" ],
    [ "Edge.cpp", "dd/de9/a00552.html", null ],
    [ "Edge.h", "d0/d79/a00553.html", "d0/d79/a00553" ],
    [ "EditableCellItem.cpp", "dc/df9/a00554.html", null ],
    [ "EditableCellItem.h", "d8/d75/a00555.html", [
      [ "EditableCellItem", "d9/d38/a00166.html", "d9/d38/a00166" ]
    ] ],
    [ "EditableEdgeItem.cpp", "d6/d95/a00556.html", null ],
    [ "EditableEdgeItem.h", "d8/df0/a00557.html", [
      [ "EditableEdgeItem", "dc/de6/a00167.html", "dc/de6/a00167" ]
    ] ],
    [ "EditableItem.h", "dc/dea/a00558.html", [
      [ "EditableItem", "dd/da7/a00168.html", "dd/da7/a00168" ]
    ] ],
    [ "EditableMesh.cpp", "d2/d50/a00559.html", null ],
    [ "EditableMesh.h", "d9/d19/a00560.html", [
      [ "EditableMesh", "de/d53/a00169.html", "de/d53/a00169" ]
    ] ],
    [ "EditableNodeItem.cpp", "d2/d50/a00561.html", null ],
    [ "EditableNodeItem.h", "d3/d2f/a00562.html", [
      [ "EditableNodeItem", "d5/dcb/a00170.html", "d5/dcb/a00170" ]
    ] ],
    [ "delta_hamiltonian/ElasticWall.cpp", "dc/dae/a00563.html", null ],
    [ "hamiltonian/ElasticWall.cpp", "d0/dc2/a00564.html", null ],
    [ "delta_hamiltonian/ElasticWall.h", "d8/dd2/a00565.html", [
      [ "ElasticWall", "d0/d01/a00339.html", "d0/d01/a00339" ]
    ] ],
    [ "hamiltonian/ElasticWall.h", "d0/d3e/a00566.html", [
      [ "ElasticWall", "d7/dfc/a00349.html", "d7/dfc/a00349" ]
    ] ],
    [ "EnabledActions.cpp", "d2/dd7/a00567.html", null ],
    [ "EnabledActions.h", "dc/d40/a00568.html", [
      [ "EnabledActions", "d6/d42/a00043.html", "d6/d42/a00043" ]
    ] ],
    [ "Exception.h", "d4/d9e/a00569.html", [
      [ "Exception", "df/dcd/a00127.html", "df/dcd/a00127" ]
    ] ],
    [ "Exploration.cpp", "d8/da5/a00570.html", null ],
    [ "Exploration.h", "df/d3a/a00571.html", [
      [ "Exploration", "d1/d0d/a00171.html", "d1/d0d/a00171" ]
    ] ],
    [ "ExplorationManager.cpp", "d6/d06/a00572.html", null ],
    [ "ExplorationManager.h", "d6/d21/a00573.html", [
      [ "ExplorationManager", "d6/dfb/a00172.html", "d6/dfb/a00172" ]
    ] ],
    [ "ExplorationProgress.cpp", "d4/dbe/a00574.html", null ],
    [ "ExplorationProgress.h", "d9/d42/a00575.html", [
      [ "ExplorationProgress", "d7/d3f/a00173.html", "d7/d3f/a00173" ]
    ] ],
    [ "ExplorationSelection.cpp", "de/d7c/a00576.html", null ],
    [ "ExplorationSelection.h", "d8/d9b/a00577.html", [
      [ "ExplorationSelection", "d8/df1/a00174.html", "d8/df1/a00174" ]
    ] ],
    [ "ExplorationTask.cpp", "dd/d5c/a00578.html", null ],
    [ "ExplorationTask.h", "dc/d4a/a00579.html", "dc/d4a/a00579" ],
    [ "ExplorationWizard.cpp", "d3/de2/a00580.html", null ],
    [ "ExplorationWizard.h", "d8/dc8/a00581.html", [
      [ "ExplorationWizard", "d5/de9/a00176.html", "d5/de9/a00176" ]
    ] ],
    [ "ExportActions.cpp", "d4/d2f/a00582.html", null ],
    [ "ExportActions.h", "d5/d80/a00583.html", [
      [ "ExportActions", "d9/d2e/a00056.html", "d9/d2e/a00056" ]
    ] ],
    [ "cell_chemistry/Factory.cpp", "d7/d21/a00584.html", null ],
    [ "cell_daughters/Factory.cpp", "dc/dac/a00585.html", null ],
    [ "cell_housekeep/Factory.cpp", "d6/d92/a00586.html", null ],
    [ "cell_split/Factory.cpp", "de/dde/a00587.html", null ],
    [ "delta_hamiltonian/Factory.cpp", "d8/d2e/a00588.html", null ],
    [ "hamiltonian/Factory.cpp", "d6/da3/a00589.html", null ],
    [ "move_gen/Factory.cpp", "d7/db1/a00590.html", null ],
    [ "reaction_transport/Factory.cpp", "d9/dad/a00591.html", null ],
    [ "reaction_transport_boundary_conditions/Factory.cpp", "d0/dec/a00592.html", null ],
    [ "time_evolver/Factory.cpp", "d2/da3/a00593.html", null ],
    [ "wall_boundary_conditions/Factory.cpp", "da/de4/a00594.html", null ],
    [ "wall_chemistry/Factory.cpp", "d8/de9/a00595.html", null ],
    [ "cell_chemistry/Factory.h", "dc/d25/a00596.html", [
      [ "Factory", "db/dc3/a00284.html", "db/dc3/a00284" ]
    ] ],
    [ "cell_daughters/Factory.h", "d8/dbd/a00597.html", [
      [ "Factory", "d9/d0d/a00298.html", "d9/d0d/a00298" ]
    ] ],
    [ "cell_housekeep/Factory.h", "d7/dd2/a00598.html", [
      [ "Factory", "d7/df2/a00312.html", "d7/df2/a00312" ]
    ] ],
    [ "cell_split/Factory.h", "d5/d8a/a00599.html", [
      [ "Factory", "d8/d23/a00326.html", "d8/d23/a00326" ]
    ] ],
    [ "delta_hamiltonian/Factory.h", "d1/d8c/a00600.html", [
      [ "Factory", "df/df1/a00340.html", "df/df1/a00340" ]
    ] ],
    [ "hamiltonian/Factory.h", "d6/db2/a00601.html", [
      [ "Factory", "db/d10/a00350.html", "db/d10/a00350" ]
    ] ],
    [ "move_gen/Factory.h", "d5/d0a/a00602.html", [
      [ "Factory", "dc/d59/a00364.html", "dc/d59/a00364" ]
    ] ],
    [ "reaction_transport/Factory.h", "de/d94/a00603.html", [
      [ "Factory", "db/db9/a00387.html", "db/db9/a00387" ]
    ] ],
    [ "reaction_transport_boundary_conditions/Factory.h", "d6/d75/a00604.html", [
      [ "Factory", "d9/da1/a00396.html", "d9/da1/a00396" ]
    ] ],
    [ "time_evolver/Factory.h", "d0/d1c/a00605.html", [
      [ "Factory", "d9/de6/a00404.html", "d9/de6/a00404" ]
    ] ],
    [ "wall_boundary_conditions/Factory.h", "d8/d00/a00606.html", [
      [ "Factory", "da/d9c/a00413.html", "da/d9c/a00413" ]
    ] ],
    [ "wall_chemistry/Factory.h", "d9/d07/a00607.html", [
      [ "Factory", "d8/de7/a00417.html", "d8/de7/a00417" ]
    ] ],
    [ "FileExploration.cpp", "d4/d14/a00608.html", null ],
    [ "FileExploration.h", "db/d3f/a00609.html", [
      [ "FileExploration", "dd/d4c/a00177.html", "dd/d4c/a00177" ]
    ] ],
    [ "FilesPage.cpp", "da/d6e/a00610.html", null ],
    [ "FilesPage.h", "dd/dc0/a00611.html", [
      [ "FilesPage", "d9/d90/a00178.html", "d9/d90/a00178" ]
    ] ],
    [ "FileSys.cpp", "d0/d8d/a00612.html", null ],
    [ "FileSys.h", "d1/d6e/a00613.html", [
      [ "InstallDirs", "da/d2a/a00086.html", "da/d2a/a00086" ]
    ] ],
    [ "FileSystemWatcher.h", "d7/d80/a00614.html", [
      [ "FileSystemWatcher", "d2/da3/a00115.html", "d2/da3/a00115" ]
    ] ],
    [ "FileViewer.h", "d5/dd8/a00615.html", [
      [ "FileViewer", "de/d21/a00006.html", "de/d21/a00006" ]
    ] ],
    [ "FileViewerPreferences.h", "db/db5/a00616.html", [
      [ "FileViewerPreferences", "db/dd9/a00179.html", "db/dd9/a00179" ]
    ] ],
    [ "cell_chemistry/Function.h", "d4/dc4/a00617.html", "d4/dc4/a00617" ],
    [ "cell_daughters/Function.h", "de/d85/a00618.html", "de/d85/a00618" ],
    [ "cell_housekeep/Function.h", "d6/d6a/a00619.html", "d6/d6a/a00619" ],
    [ "cell_split/Function.h", "dc/d4d/a00620.html", "dc/d4d/a00620" ],
    [ "delta_hamiltonian/Function.h", "dc/db1/a00621.html", "dc/db1/a00621" ],
    [ "hamiltonian/Function.h", "d0/db1/a00622.html", "d0/db1/a00622" ],
    [ "move_gen/Function.h", "df/d6f/a00623.html", "df/d6f/a00623" ],
    [ "reaction_transport/Function.h", "d3/dd6/a00624.html", "d3/dd6/a00624" ],
    [ "reaction_transport_boundary_conditions/Function.h", "d2/d34/a00625.html", "d2/d34/a00625" ],
    [ "time_evolver/Function.h", "d6/dfa/a00626.html", "d6/dfa/a00626" ],
    [ "wall_boundary_conditions/Function.h", "d3/da3/a00627.html", "d3/da3/a00627" ],
    [ "wall_chemistry/Function.h", "d1/d51/a00628.html", "d1/d51/a00628" ],
    [ "FunctionMap.h", "da/dd1/a00629.html", [
      [ "FunctionMap", "de/d5e/a00007.html", "de/d5e/a00007" ]
    ] ],
    [ "GeoData.h", "dc/d66/a00630.html", "dc/d66/a00630" ],
    [ "Geom.cpp", "d5/de3/a00631.html", null ],
    [ "Geom.h", "d4/d2a/a00632.html", [
      [ "Geom", "df/d03/a00348.html", "df/d03/a00348" ]
    ] ],
    [ "cell_housekeep/Geometric.cpp", "d9/d1d/a00633.html", null ],
    [ "cell_split/Geometric.cpp", "d7/de4/a00634.html", null ],
    [ "cell_housekeep/Geometric.h", "d4/d10/a00635.html", [
      [ "Geometric", "dd/db3/a00313.html", "dd/db3/a00313" ]
    ] ],
    [ "cell_split/Geometric.h", "db/ddc/a00636.html", [
      [ "Geometric", "d8/dfc/a00327.html", "d8/dfc/a00327" ]
    ] ],
    [ "GNUC_VERSION.h", "de/dbb/a00637.html", null ],
    [ "GraphicsPreferences.h", "d1/daa/a00638.html", [
      [ "GraphicsPreferences", "d0/d81/a00180.html", "d0/d81/a00180" ]
    ] ],
    [ "Grow.cpp", "dd/db6/a00639.html", null ],
    [ "Grow.h", "dc/d62/a00640.html", [
      [ "Grow", "de/de8/a00405.html", "de/de8/a00405" ]
    ] ],
    [ "HasUnsavedChanges.cpp", "db/d3a/a00641.html", null ],
    [ "HasUnsavedChanges.h", "df/dcf/a00642.html", [
      [ "HasUnsavedChanges", "d5/df9/a00044.html", "d5/df9/a00044" ],
      [ "HasUnsavedChangesDummy", "d6/d97/a00045.html", "d6/d97/a00045" ]
    ] ],
    [ "HasUnsavedChangesPrompt.cpp", "de/d19/a00643.html", null ],
    [ "HasUnsavedChangesPrompt.h", "d9/d0f/a00644.html", [
      [ "HasUnsavedChangesPrompt", "d3/dea/a00046.html", "d3/dea/a00046" ]
    ] ],
    [ "Hdf5Exporter.cpp", "d6/dcf/a00645.html", null ],
    [ "Hdf5Exporter.h", "d6/d08/a00646.html", [
      [ "Hdf5Exporter", "dc/d52/a00183.html", "dc/d52/a00183" ]
    ] ],
    [ "Hdf5File.cpp", "d3/d8e/a00647.html", null ],
    [ "Hdf5File.h", "dd/d1f/a00648.html", [
      [ "Hdf5File", "d2/d94/a00184.html", "d2/d94/a00184" ]
    ] ],
    [ "Hdf5Viewer.cpp", "dd/d6b/a00649.html", null ],
    [ "Hdf5Viewer.h", "da/d36/a00650.html", [
      [ "Hdf5Viewer", "d8/d00/a00186.html", "d8/d00/a00186" ]
    ] ],
    [ "HexagonalTile.cpp", "de/d33/a00651.html", null ],
    [ "HexagonalTile.h", "df/d37/a00652.html", [
      [ "HexagonalTile", "d6/daa/a00187.html", "d6/daa/a00187" ]
    ] ],
    [ "HHelper.h", "de/ddd/a00653.html", [
      [ "HHelper", "d0/d48/a00351.html", "d0/d48/a00351" ]
    ] ],
    [ "Housekeep.cpp", "d9/d2a/a00654.html", null ],
    [ "Housekeep.h", "d8/ddc/a00655.html", [
      [ "Housekeep", "d1/d8b/a00406.html", "d1/d8b/a00406" ]
    ] ],
    [ "HousekeepGrow.cpp", "dc/df2/a00656.html", null ],
    [ "HousekeepGrow.h", "da/dc3/a00657.html", [
      [ "HousekeepGrow", "d1/d98/a00407.html", "d1/d98/a00407" ]
    ] ],
    [ "hsv.h", "d9/db8/a00658.html", "d9/db8/a00658" ],
    [ "IConverterFormat.h", "dc/d91/a00659.html", [
      [ "IConverterFormat", "da/d15/a00188.html", "da/d15/a00188" ]
    ] ],
    [ "ICoupler.h", "d5/d4e/a00660.html", [
      [ "ICoupler", "d3/dc9/a00355.html", "d3/dc9/a00355" ]
    ] ],
    [ "IFactory.h", "d0/d50/a00661.html", [
      [ "IFactory", "d6/d2d/a00047.html", "d6/d2d/a00047" ]
    ] ],
    [ "IFile.h", "dc/df1/a00662.html", [
      [ "IFile", "d0/dc2/a00104.html", "d0/dc2/a00104" ]
    ] ],
    [ "IHasPTreeState.h", "d7/d5f/a00663.html", [
      [ "IHasPTreeState", "d4/dca/a00048.html", "d4/dca/a00048" ]
    ] ],
    [ "IndividualRecords.h", "df/d46/a00664.html", "df/d46/a00664" ],
    [ "IPreferences.h", "da/d46/a00665.html", [
      [ "IPreferences", "d3/d32/a00105.html", "d3/d32/a00105" ]
    ] ],
    [ "IProject.h", "d9/d41/a00666.html", [
      [ "IProject", "d7/d70/a00106.html", "d7/d70/a00106" ],
      [ "WidgetCallback", "df/d98/a00107.html", "df/d98/a00107" ]
    ] ],
    [ "ISession.h", "d2/d6e/a00667.html", [
      [ "ISession", "db/ddf/a00088.html", "db/ddf/a00088" ],
      [ "ExporterType", "db/dab/a00089.html", "db/dab/a00089" ]
    ] ],
    [ "ISweep.h", "d0/d5c/a00668.html", [
      [ "ISweep", "da/deb/a00189.html", "da/deb/a00189" ]
    ] ],
    [ "IUAP_M_Coupled.cpp", "d8/d7a/a00669.html", null ],
    [ "IUAP_M_Coupled.h", "d1/d97/a00670.html", [
      [ "IUAP_M_Coupled", "d7/d93/a00397.html", "d7/d93/a00397" ]
    ] ],
    [ "IUAP_W_Coupled.cpp", "d8/d13/a00671.html", null ],
    [ "IUAP_W_Coupled.h", "d5/d40/a00672.html", [
      [ "IUAP_W_Coupled", "d9/dd1/a00398.html", "d9/dd1/a00398" ]
    ] ],
    [ "IUserData.h", "d4/d4f/a00673.html", [
      [ "IUserData", "d1/dd6/a00108.html", "d1/dd6/a00108" ]
    ] ],
    [ "IViewerNode.h", "d8/d73/a00674.html", [
      [ "IViewerNode", "d1/dc1/a00092.html", "d1/dc1/a00092" ],
      [ "IViewerNodeWithParent", "da/da0/a00009.html", "da/da0/a00009" ]
    ] ],
    [ "IVleafSession.h", "d6/da1/a00675.html", [
      [ "IVleafSession", "dc/d55/a00226.html", "dc/d55/a00226" ]
    ] ],
    [ "IVleafSim.h", "d5/d39/a00676.html", [
      [ "IVleafSim", "d6/de1/a00356.html", "d6/de1/a00356" ]
    ] ],
    [ "IWorkspace.h", "dc/de4/a00677.html", [
      [ "IWorkspace", "dd/d42/a00109.html", "dd/d42/a00109" ],
      [ "ProjectMapEntry", "d4/d81/a00110.html", "d4/d81/a00110" ]
    ] ],
    [ "IWorkspaceFactory.h", "da/d31/a00678.html", [
      [ "IWorkspaceFactory", "dd/d6d/a00111.html", "dd/d6d/a00111" ]
    ] ],
    [ "LeafControlLogic.cpp", "d2/d85/a00679.html", null ],
    [ "LeafControlLogic.h", "d4/d9b/a00680.html", [
      [ "LeafControlLogic", "d7/d6e/a00190.html", "d7/d6e/a00190" ]
    ] ],
    [ "LeafEditor.cpp", "d8/dba/a00681.html", null ],
    [ "LeafEditor.h", "d7/dc3/a00682.html", [
      [ "LeafEditor", "d9/dd0/a00191.html", "d9/dd0/a00191" ]
    ] ],
    [ "LeafEditorActions.cpp", "db/d77/a00683.html", null ],
    [ "LeafEditorActions.h", "d3/db4/a00684.html", [
      [ "LeafEditorActions", "dd/da2/a00192.html", "dd/da2/a00192" ]
    ] ],
    [ "LeafGraphicsView.cpp", "d0/df1/a00685.html", null ],
    [ "LeafGraphicsView.h", "df/d9e/a00686.html", "df/d9e/a00686" ],
    [ "LeafSlicer.cpp", "df/d1b/a00687.html", null ],
    [ "LeafSlicer.h", "d6/dd3/a00688.html", [
      [ "LeafSlicer", "dc/d57/a00194.html", "dc/d57/a00194" ]
    ] ],
    [ "ListSweep.cpp", "d1/d87/a00689.html", null ],
    [ "ListSweep.h", "da/dd4/a00690.html", [
      [ "ListSweep", "d3/d2a/a00195.html", "d3/d2a/a00195" ]
    ] ],
    [ "log_debug.h", "db/dde/a00691.html", "db/dde/a00691" ],
    [ "LogViewer.h", "d0/db6/a00692.html", [
      [ "LogViewer", "d7/ded/a00196.html", "d7/ded/a00196" ]
    ] ],
    [ "LogWindow.cpp", "d3/dd2/a00693.html", null ],
    [ "LogWindow.h", "da/d3a/a00694.html", [
      [ "LogWindow", "d2/dee/a00049.html", "d2/dee/a00049" ]
    ] ],
    [ "LogWindowViewer.cpp", "d5/d5b/a00695.html", null ],
    [ "LogWindowViewer.h", "df/d7d/a00696.html", [
      [ "LogWindowViewer", "de/db1/a00197.html", "de/db1/a00197" ]
    ] ],
    [ "mainpage.doxy", "d2/d74/a00697.html", null ],
    [ "MaizeCC.cpp", "df/dbb/a00698.html", null ],
    [ "MaizeCC.h", "d8/d33/a00699.html", [
      [ "MaizeCC", "d3/d58/a00285.html", "d3/d58/a00285" ]
    ] ],
    [ "MaizeCD.cpp", "d0/d15/a00700.html", null ],
    [ "MaizeCD.h", "dd/d57/a00701.html", [
      [ "MaizeCD", "df/d95/a00299.html", "df/d95/a00299" ]
    ] ],
    [ "MaizeCH.cpp", "dd/d85/a00702.html", null ],
    [ "MaizeCH.h", "d8/d86/a00703.html", [
      [ "MaizeCH", "d0/d39/a00314.html", "d0/d39/a00314" ]
    ] ],
    [ "MaizeColor.cpp", "d4/d6a/a00704.html", null ],
    [ "MaizeColor.h", "d7/df1/a00705.html", [
      [ "MaizeColor", "d7/dce/a00143.html", "d7/dce/a00143" ]
    ] ],
    [ "MaizeCS.cpp", "d8/dcb/a00706.html", null ],
    [ "MaizeCS.h", "d1/d56/a00707.html", [
      [ "MaizeCS", "d4/db5/a00328.html", "d4/db5/a00328" ]
    ] ],
    [ "MaizeExpansionCH.cpp", "da/d6b/a00708.html", null ],
    [ "MaizeExpansionCH.h", "d7/d7e/a00709.html", [
      [ "MaizeExpansionCH", "d4/d6f/a00315.html", "d4/d6f/a00315" ]
    ] ],
    [ "MaizeGRNCC.cpp", "d4/deb/a00710.html", null ],
    [ "MaizeGRNCC.h", "de/d4c/a00711.html", [
      [ "MaizeGRNCC", "d5/d7b/a00286.html", "d5/d7b/a00286" ]
    ] ],
    [ "MaizeGRNCD.cpp", "d3/dda/a00712.html", null ],
    [ "MaizeGRNCD.h", "d1/d04/a00713.html", [
      [ "MaizeGRNCD", "db/da2/a00300.html", "db/da2/a00300" ]
    ] ],
    [ "MaizeGRNCH.cpp", "df/d96/a00714.html", null ],
    [ "MaizeGRNCH.h", "d3/d3b/a00715.html", [
      [ "MaizeGRNCH", "d3/db8/a00316.html", "d3/db8/a00316" ]
    ] ],
    [ "MaizeGRNColor.cpp", "d2/d94/a00716.html", null ],
    [ "MaizeGRNColor.h", "d3/d82/a00717.html", [
      [ "MaizeGRNColor", "d5/da7/a00144.html", "d5/da7/a00144" ]
    ] ],
    [ "MaizeGRNCS.cpp", "dc/d8f/a00718.html", null ],
    [ "MaizeGRNCS.h", "dd/dd8/a00719.html", [
      [ "MaizeGRNCS", "dd/d4e/a00329.html", "dd/d4e/a00329" ]
    ] ],
    [ "MaizeGRNRT.cpp", "dc/d14/a00720.html", null ],
    [ "MaizeGRNRT.h", "dd/dc1/a00721.html", [
      [ "MaizeGRNRT", "d9/dc1/a00388.html", "d9/dc1/a00388" ]
    ] ],
    [ "MaizeRT.cpp", "db/d7c/a00722.html", null ],
    [ "MaizeRT.h", "de/de6/a00723.html", [
      [ "MaizeRT", "dc/dc9/a00389.html", "dc/dc9/a00389" ]
    ] ],
    [ "math.h", "d0/d72/a00724.html", null ],
    [ "delta_hamiltonian/Maxwell.cpp", "d8/db9/a00725.html", null ],
    [ "hamiltonian/Maxwell.cpp", "d6/d59/a00726.html", null ],
    [ "delta_hamiltonian/Maxwell.h", "dc/ddc/a00727.html", [
      [ "Maxwell", "d3/d93/a00341.html", "d3/d93/a00341" ]
    ] ],
    [ "hamiltonian/Maxwell.h", "df/d3b/a00728.html", [
      [ "Maxwell", "d6/d9c/a00352.html", "d6/d9c/a00352" ]
    ] ],
    [ "MBMBuilder.cpp", "d8/d52/a00729.html", null ],
    [ "MBMBuilder.h", "d2/db1/a00730.html", [
      [ "MBMBuilder", "df/df1/a00357.html", "df/df1/a00357" ]
    ] ],
    [ "cpp_sim/model/cell_chemistry/Meinhardt.cpp", "df/df7/a00731.html", null ],
    [ "cpp_sim/model/cell_housekeep/Meinhardt.cpp", "d4/db8/a00732.html", null ],
    [ "cpp_sim/model/reaction_transport/Meinhardt.cpp", "d4/d91/a00733.html", null ],
    [ "cpp_sim/model/wall_boundary_conditions/Meinhardt.cpp", "d6/dc0/a00734.html", null ],
    [ "cpp_sim/model/wall_chemistry/Meinhardt.cpp", "d0/dab/a00735.html", null ],
    [ "cpp_vleafshell/mesh_drawer/cell_color/Meinhardt.cpp", "dc/d85/a00736.html", null ],
    [ "cpp_sim/model/cell_chemistry/Meinhardt.h", "dd/db2/a00737.html", [
      [ "Meinhardt", "d6/d7f/a00287.html", "d6/d7f/a00287" ]
    ] ],
    [ "cpp_sim/model/cell_housekeep/Meinhardt.h", "d1/da6/a00738.html", [
      [ "Meinhardt", "da/de0/a00317.html", "da/de0/a00317" ]
    ] ],
    [ "cpp_sim/model/reaction_transport/Meinhardt.h", "da/df7/a00739.html", [
      [ "Meinhardt", "d9/d0a/a00390.html", "d9/d0a/a00390" ]
    ] ],
    [ "cpp_sim/model/wall_boundary_conditions/Meinhardt.h", "dc/dce/a00740.html", [
      [ "Meinhardt", "d4/dbc/a00414.html", "d4/dbc/a00414" ]
    ] ],
    [ "cpp_sim/model/wall_chemistry/Meinhardt.h", "da/da3/a00741.html", [
      [ "Meinhardt", "de/df7/a00418.html", "de/df7/a00418" ]
    ] ],
    [ "cpp_vleafshell/mesh_drawer/cell_color/Meinhardt.h", "d4/d8f/a00742.html", [
      [ "Meinhardt", "da/ddb/a00145.html", "da/ddb/a00145" ]
    ] ],
    [ "MercurialInfo.h", "d0/d0a/a00743.html", [
      [ "MercurialInfo", "d7/da0/a00198.html", "d7/da0/a00198" ]
    ] ],
    [ "MergedPreferences.cpp", "d5/db3/a00744.html", null ],
    [ "MergedPreferences.h", "d1/d79/a00745.html", [
      [ "MergedPreferencesChanged", "d0/d61/a00100.html", "d0/d61/a00100" ],
      [ "MergedPreferences", "db/d57/a00112.html", "db/d57/a00112" ]
    ] ],
    [ "Mesh.cpp", "d4/dfa/a00746.html", null ],
    [ "Mesh.h", "d5/d68/a00747.html", [
      [ "Mesh", "df/d73/a00358.html", "df/d73/a00358" ]
    ] ],
    [ "MeshCheck.cpp", "de/d3d/a00748.html", null ],
    [ "MeshCheck.h", "d5/de8/a00749.html", [
      [ "MeshCheck", "d4/def/a00359.html", "d4/def/a00359" ]
    ] ],
    [ "MeshDrawer.cpp", "d9/db0/a00750.html", null ],
    [ "MeshDrawer.h", "da/df2/a00751.html", [
      [ "MeshDrawer", "d8/d72/a00199.html", "d8/d72/a00199" ]
    ] ],
    [ "MeshProps.cpp", "dd/d5c/a00752.html", null ],
    [ "MeshProps.h", "dc/dd0/a00753.html", [
      [ "MeshProps", "d0/d65/a00360.html", "d0/d65/a00360" ]
    ] ],
    [ "MeshState.cpp", "d8/d4e/a00754.html", null ],
    [ "MeshState.h", "d2/ddd/a00755.html", [
      [ "MeshState", "d7/ddb/a00361.html", "d7/ddb/a00361" ]
    ] ],
    [ "mode_manager.h", "d9/d77/a00756.html", "d9/d77/a00756" ],
    [ "delta_hamiltonian/ModifiedGC.cpp", "d3/d95/a00757.html", null ],
    [ "hamiltonian/ModifiedGC.cpp", "d2/d4a/a00758.html", null ],
    [ "delta_hamiltonian/ModifiedGC.h", "d2/d87/a00759.html", [
      [ "ModifiedGC", "d2/d45/a00342.html", "d2/d45/a00342" ]
    ] ],
    [ "hamiltonian/ModifiedGC.h", "d8/db4/a00760.html", [
      [ "ModifiedGC", "db/ddc/a00353.html", "db/ddc/a00353" ]
    ] ],
    [ "MyDockWidget.cpp", "d3/d4f/a00761.html", null ],
    [ "MyDockWidget.h", "df/d5f/a00762.html", [
      [ "MyDockWidget", "d5/de0/a00050.html", "d5/de0/a00050" ]
    ] ],
    [ "MyFindDialog.cpp", "d3/de3/a00763.html", null ],
    [ "MyFindDialog.h", "d3/d2c/a00764.html", [
      [ "MyFindDialog", "d2/d79/a00051.html", "d2/d79/a00051" ]
    ] ],
    [ "MyGraphicsView.cpp", "dc/de2/a00765.html", null ],
    [ "MyGraphicsView.h", "d4/d13/a00766.html", [
      [ "MyGraphicsView", "d1/d84/a00052.html", "d1/d84/a00052" ]
    ] ],
    [ "MyTreeView.cpp", "d5/dae/a00767.html", null ],
    [ "MyTreeView.h", "d8/d20/a00768.html", [
      [ "MyTreeView", "dc/dee/a00053.html", "dc/dee/a00053" ]
    ] ],
    [ "namespaces.doxy", "dd/d51/a00769.html", null ],
    [ "NeighborNodes.cpp", "d0/d7e/a00770.html", "d0/d7e/a00770" ],
    [ "NeighborNodes.h", "dd/de5/a00771.html", "dd/de5/a00771" ],
    [ "NewProjectDialog.cpp", "d9/d9c/a00772.html", null ],
    [ "NewProjectDialog.h", "d8/dba/a00773.html", [
      [ "NewProjectDialog", "d1/dea/a00054.html", "d1/dea/a00054" ]
    ] ],
    [ "Node.cpp", "d3/dee/a00774.html", "d3/dee/a00774" ],
    [ "Node.h", "dc/d5d/a00775.html", "dc/d5d/a00775" ],
    [ "NodeAdvertiser.cpp", "d9/dd4/a00776.html", null ],
    [ "NodeAdvertiser.h", "d2/d02/a00777.html", [
      [ "NodeAdvertiser", "d3/db1/a00200.html", "d3/db1/a00200" ]
    ] ],
    [ "NodeAttributes.cpp", "d6/d17/a00778.html", null ],
    [ "NodeAttributes.h", "d3/d95/a00779.html", [
      [ "NodeAttributes", "d3/d30/a00370.html", "d3/d30/a00370" ]
    ] ],
    [ "NodeInserter.cpp", "de/d42/a00780.html", null ],
    [ "NodeInserter.h", "d6/d86/a00781.html", [
      [ "NodeInserter", "d6/d80/a00371.html", "d6/d80/a00371" ]
    ] ],
    [ "NodeItem.cpp", "da/d96/a00782.html", null ],
    [ "NodeItem.h", "dc/d0c/a00783.html", [
      [ "NodeItem", "dd/de3/a00201.html", "dd/de3/a00201" ]
    ] ],
    [ "NodeMover.cpp", "d9/d00/a00784.html", null ],
    [ "NodeMover.h", "d3/d00/a00785.html", [
      [ "NCIncidence", "df/dc2/a00367.html", "df/dc2/a00367" ],
      [ "NodeMover", "db/d7e/a00372.html", "db/d7e/a00372" ],
      [ "MetropolisInfo", "dc/de3/a00373.html", "dc/de3/a00373" ]
    ] ],
    [ "NodeProtocol.cpp", "d0/d7d/a00786.html", null ],
    [ "NodeProtocol.h", "d8/d7f/a00787.html", [
      [ "NodeProtocol", "db/d53/a00202.html", "db/d53/a00202" ]
    ] ],
    [ "NoOp.cpp", "df/d0c/a00788.html", null ],
    [ "cell_chemistry/NoOp.h", "d8/d7d/a00789.html", [
      [ "NoOp", "da/ddc/a00288.html", "da/ddc/a00288" ]
    ] ],
    [ "cell_daughters/NoOp.h", "d3/daf/a00790.html", [
      [ "NoOp", "da/d50/a00301.html", "da/d50/a00301" ]
    ] ],
    [ "cell_housekeep/NoOp.h", "d4/d45/a00791.html", [
      [ "NoOp", "d2/d41/a00318.html", "d2/d41/a00318" ]
    ] ],
    [ "cell_split/NoOp.h", "d2/d5a/a00792.html", [
      [ "NoOp", "dd/dbb/a00330.html", "dd/dbb/a00330" ]
    ] ],
    [ "reaction_transport/NoOp.h", "d4/dd7/a00793.html", [
      [ "NoOp", "db/d95/a00391.html", "db/d95/a00391" ]
    ] ],
    [ "wall_chemistry/NoOp.h", "d5/d12/a00794.html", [
      [ "NoOp", "d3/d29/a00419.html", "d3/d29/a00419" ]
    ] ],
    [ "odeint.h", "dd/d7a/a00795.html", null ],
    [ "OdeintFactories0Map.cpp", "d0/da2/a00796.html", null ],
    [ "OdeintFactories0Map.h", "da/db8/a00797.html", [
      [ "OdeintFactories0Map", "d5/db5/a00374.html", "d5/db5/a00374" ]
    ] ],
    [ "OdeintFactories2Map.cpp", "de/d79/a00798.html", null ],
    [ "OdeintFactories2Map.h", "d0/d7d/a00799.html", [
      [ "OdeintFactories2Map", "dd/dcb/a00375.html", "dd/dcb/a00375" ]
    ] ],
    [ "OdeintFactory0.h", "df/d0e/a00800.html", [
      [ "OdeintFactory0", "d6/db8/a00376.html", "d6/db8/a00376" ]
    ] ],
    [ "OdeintFactory2.h", "d7/d0a/a00801.html", [
      [ "OdeintFactory2", "d4/d13/a00377.html", "d4/d13/a00377" ]
    ] ],
    [ "OdeintTraits.h", "d5/dcd/a00802.html", [
      [ "OdeintTraits", "d7/d08/a00378.html", "d7/d08/a00378" ]
    ] ],
    [ "PanAndZoomView.cpp", "d5/d72/a00803.html", null ],
    [ "PanAndZoomView.h", "d1/dd1/a00804.html", [
      [ "PanAndZoomView", "d4/d6d/a00055.html", "d4/d6d/a00055" ]
    ] ],
    [ "ParameterExploration.cpp", "d7/dff/a00805.html", null ],
    [ "ParameterExploration.h", "d0/dad/a00806.html", [
      [ "ParameterExploration", "de/d79/a00203.html", "de/d79/a00203" ]
    ] ],
    [ "ParamPage.cpp", "df/dc7/a00807.html", null ],
    [ "ParamPage.h", "d7/dc8/a00808.html", [
      [ "ParamPage", "d1/d72/a00204.html", "d1/d72/a00204" ]
    ] ],
    [ "parex_client.cpp", "dd/d7e/a00809.html", null ],
    [ "parex_node.cpp", "d4/dfd/a00810.html", null ],
    [ "parex_server.cpp", "dd/d07/a00811.html", null ],
    [ "PathPage.cpp", "d8/db7/a00812.html", null ],
    [ "PathPage.h", "de/d9d/a00813.html", [
      [ "PathPage", "d5/d13/a00205.html", "d5/d13/a00205" ]
    ] ],
    [ "PBMBuilder.cpp", "da/d71/a00814.html", null ],
    [ "PBMBuilder.h", "d8/d2d/a00815.html", [
      [ "PBMBuilder", "d8/d95/a00379.html", "d8/d95/a00379" ]
    ] ],
    [ "Perimeter.cpp", "d7/d60/a00816.html", null ],
    [ "Perimeter.h", "dc/d5e/a00817.html", [
      [ "Perimeter", "d0/d61/a00302.html", "d0/d61/a00302" ]
    ] ],
    [ "PIN.cpp", "d3/d60/a00818.html", null ],
    [ "PIN.h", "d7/d4f/a00819.html", [
      [ "PIN", "dc/d0f/a00303.html", "dc/d0f/a00303" ]
    ] ],
    [ "PINFlux.cpp", "dc/d00/a00820.html", null ],
    [ "PINFlux.h", "d6/d8b/a00821.html", [
      [ "PINFlux", "d4/d66/a00289.html", "d4/d66/a00289" ]
    ] ],
    [ "PINFlux2.cpp", "d6/da7/a00822.html", null ],
    [ "PINFlux2.h", "d3/db4/a00823.html", [
      [ "PINFlux2", "de/d25/a00290.html", "de/d25/a00290" ]
    ] ],
    [ "Plain.cpp", "d8/d1c/a00824.html", null ],
    [ "Plain.h", "dd/d5f/a00825.html", [
      [ "Plain", "d4/d27/a00392.html", "d4/d27/a00392" ]
    ] ],
    [ "delta_hamiltonian/PlainGC.cpp", "da/d90/a00826.html", null ],
    [ "hamiltonian/PlainGC.cpp", "dc/d1b/a00827.html", null ],
    [ "delta_hamiltonian/PlainGC.h", "d8/d64/a00828.html", [
      [ "PlainGC", "d0/dc5/a00343.html", "d0/dc5/a00343" ]
    ] ],
    [ "hamiltonian/PlainGC.h", "d1/dea/a00829.html", [
      [ "PlainGC", "d0/dc3/a00354.html", "d0/dc3/a00354" ]
    ] ],
    [ "PlyExporter.cpp", "d9/daf/a00830.html", null ],
    [ "PlyExporter.h", "d7/d5f/a00831.html", [
      [ "PlyExporter", "d5/df1/a00206.html", "d5/df1/a00206" ]
    ] ],
    [ "PolygonUtils.cpp", "d0/d08/a00832.html", null ],
    [ "PolygonUtils.h", "dc/d6e/a00833.html", [
      [ "PolygonUtils", "dc/d31/a00208.html", "dc/d31/a00208" ]
    ] ],
    [ "Preferences.cpp", "d9/d02/a00834.html", null ],
    [ "Preferences.h", "dd/d70/a00835.html", [
      [ "Preferences", "d3/d00/a00113.html", "d3/d00/a00113" ]
    ] ],
    [ "PreferencesChanged.h", "d3/d13/a00836.html", [
      [ "PreferencesChanged", "d4/d3a/a00101.html", "d4/d3a/a00101" ]
    ] ],
    [ "PreferencesObserver.h", "d1/de6/a00837.html", [
      [ "PreferencesObserver", "d3/de5/a00209.html", "d3/de5/a00209" ]
    ] ],
    [ "printArgv.h", "d2/dc7/a00838.html", "d2/dc7/a00838" ],
    [ "Project.h", "df/de8/a00839.html", [
      [ "Project", "d8/d17/a00114.html", "d8/d17/a00114" ]
    ] ],
    [ "Project_def.h", "d9/d61/a00840.html", null ],
    [ "ProjectChanged.h", "d5/dac/a00841.html", [
      [ "ProjectChanged", "d9/dcc/a00102.html", "d9/dcc/a00102" ]
    ] ],
    [ "ProjectController.cpp", "d0/d11/a00842.html", null ],
    [ "ProjectController.h", "dc/d22/a00843.html", [
      [ "ProjectController", "db/d3a/a00040.html", "db/d3a/a00040" ]
    ] ],
    [ "Protocol.cpp", "d3/d69/a00844.html", null ],
    [ "Protocol.h", "d7/dcf/a00845.html", [
      [ "Protocol", "da/de7/a00210.html", "da/de7/a00210" ]
    ] ],
    [ "ptree2hdf5.cpp", "d9/d87/a00846.html", "d9/d87/a00846" ],
    [ "ptree2hdf5.h", "d4/d60/a00847.html", "d4/d60/a00847" ],
    [ "PTreeComparison.cpp", "d9/d30/a00848.html", null ],
    [ "PTreeComparison.h", "de/d37/a00849.html", [
      [ "PTreeComparison", "dd/ded/a00211.html", "dd/ded/a00211" ]
    ] ],
    [ "PTreeContainer.cpp", "dc/d0a/a00850.html", null ],
    [ "PTreeContainer.h", "df/d4c/a00851.html", [
      [ "PTreeContainer", "d3/d8b/a00059.html", "d3/d8b/a00059" ]
    ] ],
    [ "PTreeContainerPreferencesObserver.cpp", "d9/dbb/a00852.html", null ],
    [ "PTreeContainerPreferencesObserver.h", "db/deb/a00853.html", [
      [ "PTreeContainerPreferencesObserver", "d9/d73/a00060.html", "d9/d73/a00060" ]
    ] ],
    [ "PTreeEditor.cpp", "d6/d2f/a00854.html", null ],
    [ "PTreeEditor.h", "da/de6/a00855.html", [
      [ "PTreeEditor", "d8/d64/a00181.html", "d8/d64/a00181" ]
    ] ],
    [ "PTreeEditorWindow.cpp", "da/de3/a00856.html", null ],
    [ "PTreeEditorWindow.h", "d0/d82/a00857.html", [
      [ "PTreeEditorWindow", "d4/de8/a00061.html", "d4/de8/a00061" ]
    ] ],
    [ "PTreeFile.cpp", "d0/d66/a00858.html", null ],
    [ "PTreeFile.h", "dc/d8c/a00859.html", [
      [ "PTreeFile", "d1/d88/a00128.html", "d1/d88/a00128" ]
    ] ],
    [ "PTreeMenu.cpp", "d2/d5e/a00860.html", null ],
    [ "PTreeMenu.h", "dd/dd5/a00861.html", [
      [ "PTreeMenu", "df/d03/a00062.html", "df/d03/a00062" ]
    ] ],
    [ "PTreeModel.cpp", "dc/d41/a00862.html", null ],
    [ "PTreeModel.h", "d7/d49/a00863.html", [
      [ "PTreeModel", "d1/d6f/a00063.html", "d1/d6f/a00063" ],
      [ "Item", "dc/ddf/a00067.html", "dc/ddf/a00067" ]
    ] ],
    [ "PTreePanels.cpp", "d7/d1b/a00864.html", null ],
    [ "PTreePanels.h", "dd/d7b/a00865.html", [
      [ "PTreePanels", "dd/dc1/a00212.html", "dd/dc1/a00212" ]
    ] ],
    [ "PTreeQtState.cpp", "dd/db2/a00866.html", null ],
    [ "PTreeQtState.h", "d2/db8/a00867.html", [
      [ "PTreeQtState", "d1/d1e/a00087.html", "d1/d1e/a00087" ]
    ] ],
    [ "PTreeUndoCommands.cpp", "da/d5b/a00868.html", null ],
    [ "PTreeUndoCommands.h", "d3/db2/a00869.html", [
      [ "EditKeyCommand", "de/de1/a00065.html", "de/de1/a00065" ],
      [ "EditDataCommand", "df/d5f/a00064.html", "df/d5f/a00064" ],
      [ "InsertRowsCommand", "d9/db3/a00066.html", "d9/db3/a00066" ],
      [ "RemoveRowsCommand", "d4/d31/a00069.html", "d4/d31/a00069" ],
      [ "MoveRowsCommand", "da/d24/a00068.html", "da/d24/a00068" ]
    ] ],
    [ "PTreeUtils.cpp", "da/d98/a00870.html", null ],
    [ "PTreeUtils.h", "dd/de1/a00871.html", [
      [ "PTreeUtils", "d9/d47/a00129.html", "d9/d47/a00129" ]
    ] ],
    [ "PTreeView.cpp", "dc/d17/a00872.html", null ],
    [ "PTreeView.h", "d9/d22/a00873.html", [
      [ "PTreeView", "d2/db1/a00070.html", "d2/db1/a00070" ]
    ] ],
    [ "QHostAnyAddress.h", "df/d2a/a00874.html", "df/d2a/a00874" ],
    [ "QtClasses.doxy", "d1/d93/a00875.html", null ],
    [ "QtPreferences.h", "d2/dac/a00876.html", [
      [ "QtPreferences", "d1/de1/a00213.html", "d1/de1/a00213" ]
    ] ],
    [ "QtViewer.cpp", "d1/de7/a00877.html", null ],
    [ "QtViewer.h", "d5/d69/a00878.html", [
      [ "QtViewer", "d3/dd1/a00214.html", "d3/dd1/a00214" ]
    ] ],
    [ "RandomEngine.cpp", "d2/da1/a00879.html", null ],
    [ "RandomEngine.h", "d1/d8f/a00880.html", [
      [ "RandomEngine", "d8/ddd/a00380.html", "d8/ddd/a00380" ]
    ] ],
    [ "RandomEngineType.cpp", "df/d41/a00881.html", "df/d41/a00881" ],
    [ "RandomEngineType.h", "d3/d50/a00882.html", "d3/d50/a00882" ],
    [ "RangeSweep.cpp", "d7/d34/a00883.html", null ],
    [ "RangeSweep.h", "dd/d08/a00884.html", [
      [ "RangeSweep", "d3/dd5/a00215.html", "d3/dd5/a00215" ]
    ] ],
    [ "RDAT.cpp", "db/d18/a00885.html", null ],
    [ "RDAT.h", "d2/dab/a00886.html", [
      [ "RDAT", "dd/dea/a00408.html", "dd/dea/a00408" ]
    ] ],
    [ "RDATEquations.cpp", "d7/d92/a00887.html", null ],
    [ "RDATEquations.h", "dd/d94/a00888.html", [
      [ "RDATEquations", "d3/d45/a00382.html", "d3/d45/a00382" ]
    ] ],
    [ "RDATSolver.cpp", "de/dc8/a00889.html", null ],
    [ "RDATSolver.h", "d7/d46/a00890.html", [
      [ "RDATSolver", "da/dee/a00383.html", "da/dee/a00383" ]
    ] ],
    [ "RectangularTile.cpp", "de/dfc/a00891.html", null ],
    [ "RectangularTile.h", "d0/de2/a00892.html", [
      [ "RectangularTile", "d6/d43/a00216.html", "d6/d43/a00216" ]
    ] ],
    [ "ReduceCellWalls.h", "dc/d2c/a00893.html", "dc/d2c/a00893" ],
    [ "RegularGeneratorDialog.cpp", "dc/dfa/a00894.html", null ],
    [ "RegularGeneratorDialog.h", "d2/d21/a00895.html", [
      [ "RegularGeneratorDialog", "df/d24/a00217.html", "df/d24/a00217" ]
    ] ],
    [ "RegularTiling.cpp", "da/dd6/a00896.html", null ],
    [ "RegularTiling.h", "de/d01/a00897.html", [
      [ "RegularTiling", "dc/d9a/a00218.html", "dc/d9a/a00218" ]
    ] ],
    [ "RootViewerNode.cpp", "d2/d79/a00898.html", null ],
    [ "RootViewerNode.h", "d3/d8a/a00899.html", [
      [ "RootViewerNode", "d0/da7/a00250.html", "d0/da7/a00250" ]
    ] ],
    [ "SaveChangesDialog.cpp", "d9/db0/a00900.html", null ],
    [ "SaveChangesDialog.h", "d0/dac/a00901.html", [
      [ "SaveChangesDialog", "d0/d89/a00071.html", "d0/d89/a00071" ]
    ] ],
    [ "SegmentedVector.h", "d5/dd3/a00902.html", [
      [ "SegmentedVector", "de/d8c/a00036.html", "de/d8c/a00036" ]
    ] ],
    [ "SelectByIDWidget.cpp", "d4/d1c/a00903.html", null ],
    [ "SelectByIDWidget.h", "d8/d0a/a00904.html", [
      [ "SelectByIDWidget", "dd/df3/a00219.html", "dd/df3/a00219" ]
    ] ],
    [ "SendPage.cpp", "da/d5b/a00905.html", null ],
    [ "SendPage.h", "da/dd8/a00906.html", [
      [ "SendPage", "d5/d0b/a00220.html", "d5/d0b/a00220" ]
    ] ],
    [ "Server.cpp", "d5/d89/a00907.html", null ],
    [ "Server.h", "d0/d85/a00908.html", [
      [ "Server", "dd/d52/a00221.html", "dd/d52/a00221" ]
    ] ],
    [ "ServerClientProtocol.cpp", "de/dc3/a00909.html", null ],
    [ "ServerClientProtocol.h", "d4/d12/a00910.html", [
      [ "ServerClientProtocol", "d0/d24/a00222.html", "d0/d24/a00222" ]
    ] ],
    [ "ServerDialog.cpp", "d7/d12/a00911.html", null ],
    [ "ServerDialog.h", "df/da5/a00912.html", [
      [ "ServerDialog", "dc/d28/a00223.html", "dc/d28/a00223" ]
    ] ],
    [ "ServerInfo.cpp", "d9/dfb/a00913.html", null ],
    [ "ServerInfo.h", "d3/dd1/a00914.html", [
      [ "ServerInfo", "d5/ddf/a00224.html", "d5/ddf/a00224" ]
    ] ],
    [ "ServerNodeProtocol.cpp", "d2/d5e/a00915.html", null ],
    [ "ServerNodeProtocol.h", "d6/d66/a00916.html", [
      [ "ServerNodeProtocol", "dc/d98/a00225.html", "dc/d98/a00225" ]
    ] ],
    [ "SessionController.cpp", "d4/ddc/a00917.html", null ],
    [ "SessionController.h", "d0/ddf/a00918.html", [
      [ "SessionController", "dd/d32/a00041.html", "dd/d32/a00041" ]
    ] ],
    [ "signum.h", "d1/d0b/a00919.html", "d1/d0b/a00919" ],
    [ "Sim.cpp", "d6/d82/a00920.html", null ],
    [ "Sim.h", "dd/d7a/a00921.html", [
      [ "Sim", "d1/d53/a00399.html", "d1/d53/a00399" ]
    ] ],
    [ "SimActions.cpp", "dd/dcf/a00922.html", null ],
    [ "SimActions.h", "df/de9/a00923.html", [
      [ "SimActions", "d1/d22/a00057.html", "d1/d22/a00057" ]
    ] ],
    [ "SimEvent.h", "de/d62/a00924.html", [
      [ "SimEvent", "d2/daf/a00346.html", "d2/daf/a00346" ]
    ] ],
    [ "SimEventType.h", "dc/d36/a00925.html", "dc/d36/a00925" ],
    [ "SimPhase.h", "d8/d38/a00926.html", "d8/d38/a00926" ],
    [ "SimResult.cpp", "dd/d98/a00927.html", null ],
    [ "SimResult.h", "dd/d38/a00928.html", [
      [ "SimResult", "d1/da8/a00230.html", "d1/da8/a00230" ]
    ] ],
    [ "SimState.cpp", "dc/d92/a00929.html", null ],
    [ "SimState.h", "d7/d16/a00930.html", [
      [ "SimState", "d9/d95/a00400.html", "d9/d95/a00400" ]
    ] ],
    [ "SimTask.cpp", "df/dbc/a00931.html", null ],
    [ "SimTask.h", "d8/dbb/a00932.html", [
      [ "SimTask", "df/d17/a00231.html", "df/d17/a00231" ]
    ] ],
    [ "Simulator.cpp", "d0/d5b/a00933.html", "d0/d5b/a00933" ],
    [ "Simulator.h", "d0/df0/a00934.html", [
      [ "Simulator", "d0/d53/a00232.html", "d0/d53/a00232" ]
    ] ],
    [ "SimWorker.cpp", "d8/d19/a00935.html", null ],
    [ "SimWorker.h", "dc/d2b/a00936.html", [
      [ "SimWorker", "d9/d01/a00227.html", "d9/d01/a00227" ]
    ] ],
    [ "SimWrapper.cpp", "de/db9/a00937.html", null ],
    [ "SimWrapper.h", "d5/d54/a00938.html", "d5/d54/a00938" ],
    [ "Size.cpp", "dd/d20/a00939.html", null ],
    [ "Size.h", "d7/db6/a00940.html", [
      [ "Size", "d9/db6/a00146.html", "d9/db6/a00146" ]
    ] ],
    [ "SliceItem.cpp", "d6/dad/a00941.html", null ],
    [ "SliceItem.h", "df/d12/a00942.html", [
      [ "SliceItem", "d6/de1/a00233.html", "d6/de1/a00233" ]
    ] ],
    [ "cell_chemistry/SmithPhyllotaxis.cpp", "d1/ddd/a00943.html", null ],
    [ "cell_daughters/SmithPhyllotaxis.cpp", "d9/dce/a00944.html", null ],
    [ "cell_housekeep/SmithPhyllotaxis.cpp", "d1/db0/a00945.html", null ],
    [ "reaction_transport/SmithPhyllotaxis.cpp", "da/d40/a00946.html", null ],
    [ "cell_chemistry/SmithPhyllotaxis.h", "d3/d29/a00947.html", [
      [ "SmithPhyllotaxis", "da/dd4/a00291.html", "da/dd4/a00291" ]
    ] ],
    [ "cell_daughters/SmithPhyllotaxis.h", "d8/d18/a00948.html", [
      [ "SmithPhyllotaxis", "d8/d17/a00304.html", "d8/d17/a00304" ]
    ] ],
    [ "cell_housekeep/SmithPhyllotaxis.h", "df/dc6/a00949.html", [
      [ "SmithPhyllotaxis", "de/dab/a00319.html", "de/dab/a00319" ]
    ] ],
    [ "reaction_transport/SmithPhyllotaxis.h", "df/d39/a00950.html", [
      [ "SmithPhyllotaxis", "da/dbf/a00393.html", "da/dbf/a00393" ]
    ] ],
    [ "cell_chemistry/Source.cpp", "dc/da0/a00951.html", null ],
    [ "reaction_transport/Source.cpp", "de/d49/a00952.html", null ],
    [ "cell_chemistry/Source.h", "d5/da3/a00953.html", [
      [ "Source", "d8/d0a/a00292.html", "d8/d0a/a00292" ]
    ] ],
    [ "reaction_transport/Source.h", "d3/dca/a00954.html", [
      [ "Source", "d9/dad/a00394.html", "d9/dad/a00394" ]
    ] ],
    [ "StandardNormal.h", "dc/db6/a00955.html", [
      [ "StandardNormal", "dd/db5/a00365.html", "dd/db5/a00365" ]
    ] ],
    [ "StandardUniform.h", "d3/de5/a00956.html", [
      [ "StandardUniform", "d2/d3e/a00366.html", "d2/d3e/a00366" ]
    ] ],
    [ "StartPage.cpp", "d1/df7/a00957.html", null ],
    [ "StartPage.h", "df/d9c/a00958.html", [
      [ "StartPage", "da/d31/a00234.html", "da/d31/a00234" ]
    ] ],
    [ "Status.cpp", "db/d40/a00959.html", null ],
    [ "Status.h", "dd/d11/a00960.html", [
      [ "Status", "d3/d42/a00235.html", "d3/d42/a00235" ]
    ] ],
    [ "stdClasses.doxy", "da/d45/a00961.html", null ],
    [ "StepFilterProxyModel.cpp", "df/d88/a00962.html", null ],
    [ "StepFilterProxyModel.h", "de/dad/a00963.html", [
      [ "StepFilterProxyModel", "d7/d40/a00236.html", "d7/d40/a00236" ]
    ] ],
    [ "StepSelection.cpp", "da/d17/a00964.html", null ],
    [ "StepSelection.h", "d3/dcd/a00965.html", [
      [ "StepSelection", "d4/d08/a00237.html", "d4/d08/a00237" ]
    ] ],
    [ "Stopwatch.h", "d2/d6e/a00966.html", "d2/d6e/a00966" ],
    [ "StringUtils.h", "d4/d94/a00967.html", "d4/d94/a00967" ],
    [ "Subject.h", "d7/dd4/a00968.html", [
      [ "Subject", "d9/d4b/a00117.html", "d9/d4b/a00117" ],
      [ "Subject< E, std::weak_ptr< const void > >", "d6/db3/a00130.html", "d6/db3/a00130" ]
    ] ],
    [ "SubjectNode.h", "d7/dfd/a00969.html", [
      [ "SubjectNode", "d9/d77/a00095.html", "d9/d77/a00095" ],
      [ "SubjectViewerNodeWrapper", "d9/df8/a00096.html", "d9/df8/a00096" ]
    ] ],
    [ "SVIterator.h", "d0/daf/a00970.html", [
      [ "SegmentedVector", "de/d8c/a00036.html", "de/d8c/a00036" ],
      [ "SVIterator", "d4/d98/a00120.html", "d4/d98/a00120" ]
    ] ],
    [ "TaskOverview.cpp", "d0/d3c/a00971.html", null ],
    [ "TaskOverview.h", "de/d4e/a00972.html", [
      [ "TaskOverview", "da/d2e/a00238.html", "da/d2e/a00238" ]
    ] ],
    [ "TemplateFilePage.cpp", "d3/da5/a00973.html", "d3/da5/a00973" ],
    [ "TemplateFilePage.h", "df/dce/a00974.html", [
      [ "TemplateFilePage", "d0/d27/a00239.html", "d0/d27/a00239" ]
    ] ],
    [ "TextViewer.h", "db/dbf/a00975.html", [
      [ "TextViewer", "d9/d21/a00240.html", "d9/d21/a00240" ]
    ] ],
    [ "TextViewerPreferences.h", "d0/d85/a00976.html", [
      [ "TextViewerPreferences", "d6/db6/a00241.html", "d6/db6/a00241" ]
    ] ],
    [ "Tile.h", "d5/d6c/a00977.html", [
      [ "Tile", "d2/dc8/a00242.html", "d2/dc8/a00242" ]
    ] ],
    [ "Timeable.h", "d2/ddd/a00978.html", [
      [ "Timeable", "d8/d87/a00118.html", "d8/d87/a00118" ]
    ] ],
    [ "cpp_sim/sim/Timekeeper.h", "d8/d49/a00979.html", "d8/d49/a00979" ],
    [ "cpp_util/timekeeper/Timekeeper.h", "db/d20/a00980.html", null ],
    [ "TimeStamp.h", "dd/d24/a00981.html", "dd/d24/a00981" ],
    [ "TransformationWidget.cpp", "d1/d7a/a00982.html", null ],
    [ "TransformationWidget.h", "d7/d98/a00983.html", [
      [ "TransformationWidget", "d6/d08/a00244.html", "d6/d08/a00244" ]
    ] ],
    [ "TriangularTile.cpp", "d2/d4c/a00984.html", null ],
    [ "TriangularTile.h", "da/deb/a00985.html", [
      [ "TriangularTile", "d6/d22/a00245.html", "d6/d22/a00245" ]
    ] ],
    [ "UndoLeafStack.cpp", "d3/d7b/a00986.html", null ],
    [ "UndoLeafStack.h", "d4/d21/a00987.html", [
      [ "UndoLeafStack", "d4/d9a/a00246.html", "d4/d9a/a00246" ]
    ] ],
    [ "Utils.h", "d6/d48/a00988.html", [
      [ "Utils", "d7/d12/a00126.html", "d7/d12/a00126" ]
    ] ],
    [ "VectorGraphicsExporter.cpp", "d4/d95/a00989.html", null ],
    [ "VectorGraphicsExporter.h", "dd/d7a/a00990.html", [
      [ "VectorGraphicsExporter", "dd/dd7/a00248.html", "dd/dd7/a00248" ]
    ] ],
    [ "VectorGraphicsPreferences.h", "df/d2f/a00991.html", [
      [ "VectorGraphicsPreferences", "de/dbc/a00249.html", "de/dbc/a00249" ]
    ] ],
    [ "ViewerActions.cpp", "d7/d76/a00992.html", null ],
    [ "ViewerActions.h", "da/d0a/a00993.html", [
      [ "ViewerActions", "d7/df7/a00058.html", "d7/df7/a00058" ]
    ] ],
    [ "ViewerDockWidget.cpp", "d4/dd0/a00994.html", null ],
    [ "ViewerDockWidget.h", "dd/d48/a00995.html", [
      [ "ViewerDockWidget", "d6/d2c/a00072.html", "d6/d2c/a00072" ]
    ] ],
    [ "ViewerEvent.h", "d3/daa/a00996.html", [
      [ "ViewerEvent", "d9/dec/a00090.html", "d9/dec/a00090" ]
    ] ],
    [ "ViewerNode.h", "d0/d28/a00997.html", [
      [ "viewer_is_subject", "d1/d84/a00097.html", "d1/d84/a00097" ],
      [ "viewer_is_widget", "d1/d44/a00098.html", "d1/d44/a00098" ],
      [ "ViewerNode", "da/d74/a00099.html", "da/d74/a00099" ],
      [ "Register", "d4/d7d/a00093.html", "d4/d7d/a00093" ],
      [ "Register< true >", "dc/d54/a00094.html", "dc/d54/a00094" ],
      [ "InitNotifier", "da/d82/a00091.html", "da/d82/a00091" ]
    ] ],
    [ "ViewerWindow.cpp", "d9/d9a/a00998.html", null ],
    [ "ViewerWindow.h", "de/de2/a00999.html", [
      [ "ViewerWindow", "d1/d6e/a00073.html", "d1/d6e/a00073" ]
    ] ],
    [ "VLeaf1.cpp", "d7/df2/a01000.html", null ],
    [ "VLeaf1.h", "df/d59/a01001.html", [
      [ "VLeaf1", "de/d3d/a00409.html", "de/d3d/a00409" ]
    ] ],
    [ "VLeaf2.cpp", "d2/dd5/a01002.html", null ],
    [ "VLeaf2.h", "dc/d10/a01003.html", [
      [ "VLeaf2", "d8/d91/a00410.html", "d8/d91/a00410" ]
    ] ],
    [ "vleaf2_cli.cpp", "df/dbc/a01004.html", null ],
    [ "vleaf2_edit_prefs.cpp", "d7/d06/a01005.html", "d7/d06/a01005" ],
    [ "vleaf2_editor.cpp", "dc/d81/a01006.html", "dc/d81/a01006" ],
    [ "vleaf2_gui.cpp", "d6/d8d/a01007.html", null ],
    [ "vleaf2_parex.cpp", "dd/dac/a01008.html", "dd/dac/a01008" ],
    [ "vleaf2_sim.cpp", "d7/ded/a01009.html", "d7/ded/a01009" ],
    [ "vleaf_modes.h", "d0/d2b/a01010.html", [
      [ "ConverterCLIMode", "d7/d46/a00002.html", "d7/d46/a00002" ],
      [ "ConverterGUIMode", "d1/d7c/a00003.html", "d1/d7c/a00003" ],
      [ "ParexClientMode", "da/d45/a00011.html", "da/d45/a00011" ],
      [ "ParexNodeMode", "df/d86/a00012.html", "df/d86/a00012" ],
      [ "ParexServerMode", "d7/dd4/a00013.html", "d7/dd4/a00013" ],
      [ "VLeafCLIMode", "db/d4f/a00420.html", "db/d4f/a00420" ],
      [ "VLeafGUIMode", "d2/de1/a00421.html", "d2/de1/a00421" ]
    ] ],
    [ "VleafConversion.cpp", "d6/d36/a01011.html", null ],
    [ "VleafConversion.h", "d4/d45/a01012.html", [
      [ "VleafConversion", "d3/d1a/a00251.html", "d3/d1a/a00251" ]
    ] ],
    [ "VleafConverterFormats.cpp", "d7/d59/a01013.html", null ],
    [ "VleafConverterFormats.h", "d0/d56/a01014.html", [
      [ "TimeStepPostfixFormat", "d6/d82/a00243.html", "d6/d82/a00243" ],
      [ "ExporterFormat", "dd/dad/a00005.html", "dd/dad/a00005" ],
      [ "CsvFormat", "db/d44/a00162.html", "db/d44/a00162" ],
      [ "CsvGzFormat", "db/ddf/a00164.html", "db/ddf/a00164" ],
      [ "PlyFormat", "de/dd2/a00207.html", "de/dd2/a00207" ],
      [ "XmlFormat", "d9/dd6/a00272.html", "d9/dd6/a00272" ],
      [ "XmlGzFormat", "dd/d56/a00274.html", "dd/d56/a00274" ],
      [ "BitmapFormat", "d5/d5b/a00135.html", "d5/d5b/a00135" ],
      [ "VectorFormat", "d0/d1f/a00247.html", "d0/d1f/a00247" ],
      [ "Hdf5Format", "de/df3/a00185.html", "de/df3/a00185" ],
      [ "VleafConverterFormats", "dc/ded/a00252.html", "dc/ded/a00252" ]
    ] ],
    [ "VleafFactory.cpp", "d1/dac/a01015.html", null ],
    [ "VleafFactory.h", "de/def/a01016.html", [
      [ "VleafFactory", "d7/d25/a00182.html", "d7/d25/a00182" ]
    ] ],
    [ "VleafFile.cpp", "d3/ded/a01017.html", null ],
    [ "VleafFile.h", "d3/d08/a01018.html", [
      [ "VleafFile", "d9/d8d/a00262.html", "d9/d8d/a00262" ]
    ] ],
    [ "VleafFileHdf5.cpp", "db/daf/a01019.html", null ],
    [ "VleafFileHdf5.h", "d0/d43/a01020.html", [
      [ "VleafFileHdf5", "df/da9/a00263.html", "df/da9/a00263" ]
    ] ],
    [ "VleafFilePtree.cpp", "d5/dfb/a01021.html", null ],
    [ "VleafFilePtree.h", "d1/dd8/a01022.html", [
      [ "VleafFilePtree", "d3/dcc/a00264.html", "d3/dcc/a00264" ]
    ] ],
    [ "VleafFileXml.cpp", "d6/dcd/a01023.html", null ],
    [ "VleafFileXml.h", "d4/dc5/a01024.html", [
      [ "VleafFileXml", "da/d53/a00265.html", "da/d53/a00265" ]
    ] ],
    [ "VleafFileXmlGz.cpp", "db/d23/a01025.html", null ],
    [ "VleafFileXmlGz.h", "de/d02/a01026.html", [
      [ "VleafFileXmlGz", "d5/d35/a00266.html", "d5/d35/a00266" ]
    ] ],
    [ "VleafProject.cpp", "dd/dfd/a01027.html", null ],
    [ "VleafProject.h", "db/d08/a01028.html", "db/d08/a01028" ],
    [ "VleafSession.cpp", "d0/daa/a01029.html", null ],
    [ "VleafSession.h", "de/d41/a01030.html", [
      [ "VleafSession", "de/dc2/a00228.html", "de/dc2/a00228" ]
    ] ],
    [ "VleafSessionCoupled.cpp", "d6/dcf/a01031.html", null ],
    [ "VleafSessionCoupled.h", "df/d89/a01032.html", [
      [ "VleafSessionCoupled", "dd/d2d/a00229.html", "dd/d2d/a00229" ]
    ] ],
    [ "VleafWorkspace.cpp", "d2/d57/a01033.html", null ],
    [ "VleafWorkspace.h", "de/d91/a01034.html", "de/d91/a01034" ],
    [ "VleafWorkspaceFactory.cpp", "de/d26/a01035.html", null ],
    [ "VleafWorkspaceFactory.h", "d8/d79/a01036.html", [
      [ "VleafWorkspaceFactory", "d9/d2a/a00270.html", "d9/d2a/a00270" ]
    ] ],
    [ "VoronoiGeneratorDialog.cpp", "d2/d41/a01037.html", null ],
    [ "VoronoiGeneratorDialog.h", "d4/dbc/a01038.html", [
      [ "VoronoiGeneratorDialog", "d0/d3e/a00253.html", "d0/d3e/a00253" ]
    ] ],
    [ "VoronoiTesselation.cpp", "df/d21/a01039.html", null ],
    [ "VoronoiTesselation.h", "db/d14/a01040.html", [
      [ "VoronoiTesselation", "d7/d03/a00254.html", "d7/d03/a00254" ],
      [ "ClippedEdge", "d4/d04/a00255.html", "d4/d04/a00255" ]
    ] ],
    [ "Wall.cpp", "d1/d83/a01041.html", "d1/d83/a01041" ],
    [ "Wall.h", "d4/d56/a01042.html", "d4/d56/a01042" ],
    [ "WallAttributes.cpp", "d8/d27/a01043.html", null ],
    [ "WallAttributes.h", "d0/ddd/a01044.html", [
      [ "WallAttributes", "d9/d72/a00412.html", "d9/d72/a00412" ]
    ] ],
    [ "WallItem.cpp", "d7/d46/a01045.html", null ],
    [ "WallItem.h", "da/d17/a01046.html", [
      [ "WallItem", "d0/def/a00256.html", "d0/def/a00256" ]
    ] ],
    [ "WallType.cpp", "db/dbb/a01047.html", "db/dbb/a01047" ],
    [ "WallType.h", "d5/de7/a01048.html", "d5/de7/a01048" ],
    [ "WorkerNode.cpp", "d0/d22/a01049.html", null ],
    [ "WorkerNode.h", "d9/d8e/a01050.html", [
      [ "WorkerNode", "d9/d39/a00257.html", "d9/d39/a00257" ]
    ] ],
    [ "WorkerPool.cpp", "d3/d42/a01051.html", "d3/d42/a01051" ],
    [ "WorkerPool.h", "d9/dce/a01052.html", [
      [ "WorkerPool", "d7/d2f/a00258.html", "d7/d2f/a00258" ]
    ] ],
    [ "WorkerRepresentative.cpp", "dc/d99/a01053.html", null ],
    [ "WorkerRepresentative.h", "dd/d57/a01054.html", [
      [ "WorkerRepresentative", "d0/d2f/a00259.html", "d0/d2f/a00259" ]
    ] ],
    [ "Workspace.h", "d2/d71/a01055.html", [
      [ "Workspace", "d4/d3e/a00116.html", "d4/d3e/a00116" ]
    ] ],
    [ "Workspace_def.h", "dc/d19/a01056.html", null ],
    [ "WorkspaceChanged.h", "d2/db8/a01057.html", [
      [ "WorkspaceChanged", "d5/d1e/a00103.html", "d5/d1e/a00103" ]
    ] ],
    [ "WorkspaceController.cpp", "d5/d4c/a01058.html", null ],
    [ "WorkspaceController.h", "d0/d90/a01059.html", [
      [ "WorkspaceController", "d5/db6/a00042.html", "d5/db6/a00042" ]
    ] ],
    [ "WorkspaceQtModel.cpp", "d5/d16/a01060.html", null ],
    [ "WorkspaceQtModel.h", "de/d9e/a01061.html", [
      [ "WorkspaceQtModel", "db/db6/a00074.html", "db/db6/a00074" ],
      [ "Item", "d9/da5/a00075.html", "d9/da5/a00075" ],
      [ "WS", "d9/da4/a00078.html", "d9/da4/a00078" ],
      [ "PR", "d1/d09/a00077.html", "d1/d09/a00077" ],
      [ "FI", "db/df8/a00076.html", "db/df8/a00076" ]
    ] ],
    [ "WorkspaceView.cpp", "d4/d8a/a01062.html", null ],
    [ "WorkspaceView.h", "de/de6/a01063.html", [
      [ "WorkspaceView", "d1/d51/a00079.html", "d1/d51/a00079" ],
      [ "CallbackMapEntry", "da/dec/a00080.html", "da/dec/a00080" ]
    ] ],
    [ "WorkspaceWizard.cpp", "d9/db3/a01064.html", null ],
    [ "WorkspaceWizard.h", "d0/dc9/a01065.html", [
      [ "WorkspaceWizard", "d8/d53/a00081.html", "d8/d53/a00081" ],
      [ "PathPage", "df/d13/a00085.html", "df/d13/a00085" ],
      [ "ExistingPage", "d2/d1f/a00083.html", "d2/d1f/a00083" ],
      [ "InitPage", "df/d5d/a00084.html", "df/d5d/a00084" ],
      [ "DonePage", "d4/d63/a00082.html", "d4/d63/a00082" ]
    ] ],
    [ "WortelCC.cpp", "d0/dfb/a01066.html", null ],
    [ "WortelCC.h", "d2/d49/a01067.html", [
      [ "WortelCC", "d6/d82/a00293.html", "d6/d82/a00293" ]
    ] ],
    [ "WortelCD.cpp", "d1/de6/a01068.html", null ],
    [ "WortelCD.h", "d8/d18/a01069.html", [
      [ "WortelCD", "d3/d07/a00305.html", "d3/d07/a00305" ]
    ] ],
    [ "WortelCH.cpp", "d5/d67/a01070.html", null ],
    [ "WortelCH.h", "df/d18/a01071.html", [
      [ "WortelCH", "dd/de2/a00320.html", "dd/de2/a00320" ]
    ] ],
    [ "WortelCHCoupled.cpp", "d6/d6a/a01072.html", null ],
    [ "WortelCHCoupled.h", "d9/d7c/a01073.html", [
      [ "WortelCHCoupled", "db/d53/a00321.html", "db/d53/a00321" ]
    ] ],
    [ "WortelColor.cpp", "d7/df7/a01074.html", null ],
    [ "WortelColor.h", "dd/d80/a01075.html", [
      [ "WortelColor", "d0/d4a/a00147.html", "d0/d4a/a00147" ]
    ] ],
    [ "WortelCS.cpp", "d6/d80/a01076.html", null ],
    [ "WortelCS.h", "d0/dd3/a01077.html", [
      [ "WortelCS", "d8/df7/a00331.html", "d8/df7/a00331" ]
    ] ],
    [ "WortelLightCC.cpp", "db/dba/a01078.html", null ],
    [ "WortelLightCC.h", "d2/d4d/a01079.html", [
      [ "WortelLightCC", "d4/d5b/a00294.html", "d4/d5b/a00294" ]
    ] ],
    [ "WortelLightCD.cpp", "d9/d67/a01080.html", null ],
    [ "WortelLightCD.h", "dd/dee/a01081.html", [
      [ "WortelLightCD", "d1/de3/a00306.html", "d1/de3/a00306" ]
    ] ],
    [ "WortelLightCS.cpp", "dd/df1/a01082.html", null ],
    [ "WortelLightCS.h", "d6/d9a/a01083.html", [
      [ "WortelLightCS", "dc/d50/a00332.html", "dc/d50/a00332" ]
    ] ],
    [ "WortelRT.cpp", "da/d93/a01084.html", null ],
    [ "WortelRT.h", "d2/df3/a01085.html", [
      [ "WortelRT", "db/dbc/a00395.html", "db/dbc/a00395" ]
    ] ],
    [ "XmlExporter.cpp", "d9/df5/a01086.html", null ],
    [ "XmlExporter.h", "d0/d1f/a01087.html", [
      [ "XmlExporter", "dd/d86/a00271.html", "dd/d86/a00271" ]
    ] ],
    [ "XmlGzExporter.cpp", "da/db7/a01088.html", null ],
    [ "XmlGzExporter.h", "d3/dc4/a01089.html", [
      [ "XmlGzExporter", "d0/d19/a00273.html", "d0/d19/a00273" ]
    ] ],
    [ "XmlViewer.cpp", "da/d73/a01090.html", null ],
    [ "XmlViewer.h", "d6/d0d/a01091.html", "d6/d0d/a01091" ],
    [ "XmlWriterSettings.h", "de/d9c/a01092.html", [
      [ "XmlWriterSettings", "d7/d06/a00131.html", "d7/d06/a00131" ]
    ] ]
];