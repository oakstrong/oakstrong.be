var a00208 =
[
    [ "IsClockwise", "dc/d31/a00208.html#af8a4cbf56577f8be8b2decc815dc92a9", null ],
    [ "IsSimplePolygon", "dc/d31/a00208.html#a8051f4b51b7cfd170983964355aeb4b2", null ],
    [ "OpenPolygon", "dc/d31/a00208.html#aa2bcf2520682bfa719e891cbfd171822", null ],
    [ "Counterclockwise", "dc/d31/a00208.html#add55b28c3b4b4ec86c492c487211e050", null ],
    [ "CalculateArea", "dc/d31/a00208.html#a106381d2b0fefe7b0c830f398a2a6bf4", null ],
    [ "ClipPolygon", "dc/d31/a00208.html#ab33e38d3c2e8b6c513162f6d778d4779", null ],
    [ "SlicePolygon", "dc/d31/a00208.html#a789807fc258d8e71d28013b21fcd1b63", null ],
    [ "Turn", "dc/d31/a00208.html#a2ed8da08d72e08a25249e37bf4439c4c", null ],
    [ "g_accuracy", "dc/d31/a00208.html#a4e5c99961af453b0c8f74ea8b4875f4c", null ]
];