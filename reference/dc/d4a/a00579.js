var a00579 =
[
    [ "ExplorationTask", "de/d45/a00175.html", "de/d45/a00175" ],
    [ "TaskState", "dc/d4a/a00579.html#abb6cecd6ae11cd968574ab0e2cb4c1df", [
      [ "Waiting", "dc/d4a/a00579.html#abb6cecd6ae11cd968574ab0e2cb4c1dfa5706de961fb376d701be6e7762d8b09c", null ],
      [ "Running", "dc/d4a/a00579.html#abb6cecd6ae11cd968574ab0e2cb4c1dfa5bda814c4aedb126839228f1a3d92f09", null ],
      [ "Finished", "dc/d4a/a00579.html#abb6cecd6ae11cd968574ab0e2cb4c1dfa8f3d10eb21bd36347c258679eba9e92b", null ],
      [ "Cancelled", "dc/d4a/a00579.html#abb6cecd6ae11cd968574ab0e2cb4c1dfaa149e85a44aeec9140e92733d9ed694e", null ],
      [ "Failed", "dc/d4a/a00579.html#abb6cecd6ae11cd968574ab0e2cb4c1dfad7c8c85bf79bbe1b7188497c32c3b0ca", null ]
    ] ]
];