var a00223 =
[
    [ "ServerDialog", "dc/d28/a00223.html#a9039bf0e4fa38a6a4a5f585702b69642", null ],
    [ "~ServerDialog", "dc/d28/a00223.html#af91839df7facc699e13b49dca5eeaa2a", null ],
    [ "GetServer", "dc/d28/a00223.html#a571204a4ba0ba20460cdfcf9e6f3fc54", null ],
    [ "LoadServerList", "dc/d28/a00223.html#a15dbdb33d73a44fc2382807c8bd95c31", null ],
    [ "SaveServerList", "dc/d28/a00223.html#af32a7ab9a78817f1c5710dbeaa652c51", null ],
    [ "Connect", "dc/d28/a00223.html#ad8c3060d0aff54ff364821d604715b19", null ],
    [ "SelectionChanged", "dc/d28/a00223.html#ac4d1e442abb0178901000bc292f2d861", null ],
    [ "UpdateName", "dc/d28/a00223.html#a93aa99f4f40123c4032aa7b3f9ed355d", null ],
    [ "UpdateAddress", "dc/d28/a00223.html#a1faf326231b797c879460e8e226ddcfb", null ],
    [ "UpdatePort", "dc/d28/a00223.html#a257555f944fcb987aa62d64d72d70ee7", null ],
    [ "SaveServer", "dc/d28/a00223.html#a0cf78e0532b25f0e41450ca146fd421b", null ],
    [ "DeleteServer", "dc/d28/a00223.html#aa72c64d7828e6511acf0ed39fb5c0e24", null ],
    [ "m_name", "dc/d28/a00223.html#a627e645c9fb4baa32603fb213b5a785a", null ],
    [ "m_address", "dc/d28/a00223.html#a8cc2514d8a0c408446bdd407588ab150", null ],
    [ "m_port", "dc/d28/a00223.html#af46cf5889db95ff6e4232acdca86b585", null ],
    [ "m_path", "dc/d28/a00223.html#a2a46678038e2756fa1ee38d378cbb333", null ],
    [ "m_servers", "dc/d28/a00223.html#a1b1530476f2377508a8794c4a8a6d216", null ],
    [ "m_current_server", "dc/d28/a00223.html#af7c85e8d1934d886363d45218163a21a", null ],
    [ "m_last_server", "dc/d28/a00223.html#ab283a6657b4ba4745ce00323f083448c", null ],
    [ "m_server_list", "dc/d28/a00223.html#a47ef6eded5a2cfa2c8d01cdbe18dcf36", null ]
];