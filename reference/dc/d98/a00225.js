var a00225 =
[
    [ "ServerNodeProtocol", "dc/d98/a00225.html#a01fff271f3a7e0800bc76e7f9e8ba36b", null ],
    [ "~ServerNodeProtocol", "dc/d98/a00225.html#a8269145b73a0dc48df73e31c6d9506bf", null ],
    [ "SendTask", "dc/d98/a00225.html#a741dea2d35058273b249fbbe6ca06ebf", null ],
    [ "SendAck", "dc/d98/a00225.html#ade3400ba9dba39faa15f86ecac111c6b", null ],
    [ "StopTask", "dc/d98/a00225.html#ae6dc0227abb94d6499cb49ca45291185", null ],
    [ "Delete", "dc/d98/a00225.html#a1ae702ed84746beee27a4c6864ca19cb", null ],
    [ "ResultReceived", "dc/d98/a00225.html#a21924536f885503cddc6aa7c1f13a223", null ],
    [ "ReceivePtree", "dc/d98/a00225.html#a0ce9b77bf62a8990143326665143e055", null ]
];