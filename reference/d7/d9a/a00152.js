var a00152 =
[
    [ "ClientProtocol", "d7/d9a/a00152.html#a57c5305b9375f805ba8beec4e42b2110", null ],
    [ "~ClientProtocol", "d7/d9a/a00152.html#a7fd5f2ce50f6356e0c0670d39e6ecbca", null ],
    [ "SendExploration", "d7/d9a/a00152.html#a4c8e8d826dd5344871011ed274bbfcbe", null ],
    [ "DeleteExploration", "d7/d9a/a00152.html#af0ae048a8fd28a54115b5296de5b8619", null ],
    [ "RequestExplorationNames", "d7/d9a/a00152.html#ae5350f887d60613ecd1debe17883df88", null ],
    [ "SubscribeUpdates", "d7/d9a/a00152.html#a046c87d6bc758b733b7f964076b74bf9", null ],
    [ "UnsubscribeUpdates", "d7/d9a/a00152.html#ac8da8656ea21f80d58d6d7326b7f9392", null ],
    [ "StopTask", "d7/d9a/a00152.html#af734cfd04e2de1bf7fac380905fd7343", null ],
    [ "RestartTask", "d7/d9a/a00152.html#a3231dd9f64556e3cca9aae28996f1d2c", null ],
    [ "ExplorationNames", "d7/d9a/a00152.html#aeb6f880e34c925322458f51ab2b78eff", null ],
    [ "ExplorationStatus", "d7/d9a/a00152.html#a32c24394879fbc5f90698e28f50442a9", null ],
    [ "ExplorationDeleted", "d7/d9a/a00152.html#a820f72b45103994abfe693557d26ef6b", null ],
    [ "ReceivePtree", "d7/d9a/a00152.html#a5e8037cdb10fce3651ee6d40d54079c7", null ]
];