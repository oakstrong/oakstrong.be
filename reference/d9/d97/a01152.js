var a01152 =
[
    [ "AreaThresholdBased", "de/d55/a00323.html", "de/d55/a00323" ],
    [ "AuxinGrowth", "d4/d8f/a00324.html", "d4/d8f/a00324" ],
    [ "BladCS", "de/dc8/a00325.html", "de/dc8/a00325" ],
    [ "Factory", "d8/d23/a00326.html", "d8/d23/a00326" ],
    [ "Geometric", "d8/dfc/a00327.html", "d8/dfc/a00327" ],
    [ "MaizeCS", "d4/db5/a00328.html", "d4/db5/a00328" ],
    [ "MaizeGRNCS", "dd/d4e/a00329.html", "dd/d4e/a00329" ],
    [ "NoOp", "dd/dbb/a00330.html", "dd/dbb/a00330" ],
    [ "WortelCS", "d8/df7/a00331.html", "d8/df7/a00331" ],
    [ "WortelLightCS", "dc/d50/a00332.html", "dc/d50/a00332" ]
];