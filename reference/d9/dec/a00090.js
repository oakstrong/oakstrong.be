var a00090 =
[
    [ "Source", "d9/dec/a00090.html#afc260cd0d5ccefed1b4dec085cbacac6", null ],
    [ "Type", "d9/dec/a00090.html#a82df465ff1928e024f1f6fa6706ba7ab", [
      [ "Enabled", "d9/dec/a00090.html#a82df465ff1928e024f1f6fa6706ba7abaefc8afc993e94248f77196c61f4c0e3b", null ],
      [ "Disabled", "d9/dec/a00090.html#a82df465ff1928e024f1f6fa6706ba7aba02292912b9927e06fa4d1f7e0423a01f", null ]
    ] ],
    [ "ViewerEvent", "d9/dec/a00090.html#a6420296de225c10e1fa6df3497e79b45", null ],
    [ "GetSource", "d9/dec/a00090.html#a801af35d33f9648f9b4b27fe2801501c", null ],
    [ "GetType", "d9/dec/a00090.html#a953054337dab5165cc02bcf9b74e4e80", null ],
    [ "m_source", "d9/dec/a00090.html#aac8e0dc09dbb8b0a91acf69d447381ab", null ],
    [ "m_type", "d9/dec/a00090.html#a1d6adf28ca243ec6c0fdcd95145f4228", null ]
];