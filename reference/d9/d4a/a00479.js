var a00479 =
[
    [ "BoundaryType", "d9/d4a/a00479.html#a3df437a053ea0a8b81ff2d2665ab1478", [
      [ "None", "d9/d4a/a00479.html#a3df437a053ea0a8b81ff2d2665ab1478a6adf97f83acf6453d4a6a4b1070f3754", null ],
      [ "NoFlux", "d9/d4a/a00479.html#a3df437a053ea0a8b81ff2d2665ab1478aa5af8c34dc3d5f295a4ce78e0930690d", null ],
      [ "SourceSink", "d9/d4a/a00479.html#a3df437a053ea0a8b81ff2d2665ab1478a6919f0fe04176bccb463fecf7bb3a052", null ],
      [ "SAM", "d9/d4a/a00479.html#a3df437a053ea0a8b81ff2d2665ab1478a9f1b3be4a82b11d104e4ef7f7ccb1c19", null ]
    ] ],
    [ "operator<<", "d9/d4a/a00479.html#a4aa0de029ceed9172266897858ad7286", null ],
    [ "operator>>", "d9/d4a/a00479.html#af38ab87ab2ab5770d545adb33852e328", null ]
];