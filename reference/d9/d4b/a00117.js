var a00117 =
[
    [ "EventType", "d9/d4b/a00117.html#af32df1d15c033b64896877590ee985f9", null ],
    [ "KeyType", "d9/d4b/a00117.html#a627b150946f7000d21d3c13088b9c113", null ],
    [ "CallbackType", "d9/d4b/a00117.html#ad7595c11eda807eaf069dfdafad5e020", null ],
    [ "Compare", "d9/d4b/a00117.html#afd91a7d175a36c1db7c3c353662aa8e1", null ],
    [ "~Subject", "d9/d4b/a00117.html#a1936912b6b9f78edd840056a68fc194d", null ],
    [ "Register", "d9/d4b/a00117.html#acf680b89d5d11d046abb9c761b9053bd", null ],
    [ "Unregister", "d9/d4b/a00117.html#af68745ae81648921ba597f6a1cb4396c", null ],
    [ "UnregisterAll", "d9/d4b/a00117.html#a61c9fbbe4d89787d283e429bac9cb94f", null ],
    [ "Notify", "d9/d4b/a00117.html#ad902cb717d674c35776177e7646ac01b", null ],
    [ "m_observers", "d9/d4b/a00117.html#a09b34fa6c13a9e606a95d12308ad660d", null ]
];