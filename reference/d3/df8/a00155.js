var a00155 =
[
    [ "Connection", "d3/df8/a00155.html#a7ffdddee20e932143296daf100b1b389", null ],
    [ "~Connection", "d3/df8/a00155.html#a3ef9f94b1295b6a44b23b5eddb6fe1c2", null ],
    [ "Connection", "d3/df8/a00155.html#a67dc479a55412679681359347d7f4c55", null ],
    [ "IsConnected", "d3/df8/a00155.html#a2dac68e32c51c994dbfdee6a757f1138", null ],
    [ "GetPeerAddress", "d3/df8/a00155.html#aa27aa37870ca7a760203eee124741a9f", null ],
    [ "GetPeerPort", "d3/df8/a00155.html#a79988481e4e2189059280ccab29500a7", null ],
    [ "SendMessage", "d3/df8/a00155.html#a56bae118d5446ec70ea4dff4d55c617e", null ],
    [ "ReceivedMessage", "d3/df8/a00155.html#ace6c9c913935436c4a4ede913b1813d2", null ],
    [ "ConnectionClosed", "d3/df8/a00155.html#af01eff706f3d13a249e035daff4452e6", null ],
    [ "Error", "d3/df8/a00155.html#ac90ed99a683808492d2705c5ffaba717", null ],
    [ "ReadMessage", "d3/df8/a00155.html#aa5d3f0606a88995f1b65461fd6b718ed", null ],
    [ "HandleError", "d3/df8/a00155.html#a9b25adac32014f53b1d3bd03b7412bf4", null ],
    [ "operator=", "d3/df8/a00155.html#a8331ea986bba783d9cfcf741fbf694e5", null ],
    [ "m_socket", "d3/df8/a00155.html#abef686cd724625fdcc287f2035c142e2", null ],
    [ "m_size", "d3/df8/a00155.html#a64bcac83f87cac3411c99e480ae84bb9", null ]
];