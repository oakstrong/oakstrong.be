var a00113 =
[
    [ "~Preferences", "d3/d00/a00113.html#a36cb739aa1ff44c418056baf1f83dc33", null ],
    [ "Preferences", "d3/d00/a00113.html#a27f8b1e83924e7dfaa570ae31d27937d", null ],
    [ "Preferences", "d3/d00/a00113.html#a2546512687900fcdddcac516574e41cf", null ],
    [ "Preferences", "d3/d00/a00113.html#a9e5015840e8ced3c2343a74d5c9e1082", null ],
    [ "GetPreferences", "d3/d00/a00113.html#ad18702655fac03b6315e2b05d58126d5", null ],
    [ "SetPreferences", "d3/d00/a00113.html#ad694c75e24bdaadb8510fcf23c01b2a0", null ],
    [ "operator=", "d3/d00/a00113.html#aae4d2cc1089e53ae233f5486f7a153d4", null ],
    [ "m_preferences", "d3/d00/a00113.html#a45739e078c487bc45ced35f35b23963e", null ],
    [ "m_file", "d3/d00/a00113.html#a1f0ab6da6af52381c8a2d197d5e9eb1c", null ]
];