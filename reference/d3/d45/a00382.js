var a00382 =
[
    [ "RDATEquations", "d3/d45/a00382.html#a0adfeb754fbc0e02f13a7026fd6e4887", null ],
    [ "GetDerivatives", "d3/d45/a00382.html#a65127275800377753dedd1f62503066c", null ],
    [ "GetEquationCount", "d3/d45/a00382.html#a96ef153c255a2fe56e63ef029e164e2e", null ],
    [ "GetVariables", "d3/d45/a00382.html#ab63d40cc2d0cd8550ea47f64228be51f", null ],
    [ "Initialize", "d3/d45/a00382.html#a99878353efa58d47a5a218156d401935", null ],
    [ "SetVariables", "d3/d45/a00382.html#a1e96be9b6cfd515421e2744dc4322cb2", null ],
    [ "m_cd", "d3/d45/a00382.html#abb80172070eb69d3cf032a5e6f93f1e0", null ],
    [ "m_cell_dynamics", "d3/d45/a00382.html#a98aaa8c5cfe61e4c640e034579254b0a", null ],
    [ "m_effective_nchem", "d3/d45/a00382.html#af7e0e2625c8aa647a223b2aa8f07c0cb", null ],
    [ "m_equation_count", "d3/d45/a00382.html#a5c9549becdf5187ee94605254f1fdbcc", null ],
    [ "m_mesh", "d3/d45/a00382.html#a11590e0bcb83e46ae5746dbbf46c13b2", null ],
    [ "m_reaction_transport", "d3/d45/a00382.html#a9451ee6f77a942c0684fb7695aea8cd8", null ],
    [ "m_wall_dynamics", "d3/d45/a00382.html#a9c85448ee6c45a52faac793d7f5627b3", null ]
];