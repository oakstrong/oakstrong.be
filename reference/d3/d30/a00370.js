var a00370 =
[
    [ "NodeAttributes", "d3/d30/a00370.html#a3de158dca9139772ec7b851bd4c8d1be", null ],
    [ "~NodeAttributes", "d3/d30/a00370.html#a2d0598b519c939199890fc79b129b08f", null ],
    [ "IsFixed", "d3/d30/a00370.html#ae02019f5c1945c0bea301d9d47377e59", null ],
    [ "IsSam", "d3/d30/a00370.html#a6c3140cb572b673c31762040f9b42606", null ],
    [ "ReadPtree", "d3/d30/a00370.html#a3e3ce627f6f3b9396e24424ac33f01b2", null ],
    [ "SetFixed", "d3/d30/a00370.html#a54a221163864ef1f56749745c15e7e19", null ],
    [ "SetSam", "d3/d30/a00370.html#a02bc07c0755468c36ab9b72db507010a", null ],
    [ "ToPtree", "d3/d30/a00370.html#a782c6c812f6ad1e803fcf6774bd6e2ee", null ],
    [ "m_fixed", "d3/d30/a00370.html#a2ce195a1edac15553899a3648321961c", null ],
    [ "m_sam", "d3/d30/a00370.html#aa8e2bc05ee559830dc35467c577fbcbc", null ]
];