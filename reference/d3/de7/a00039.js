var a00039 =
[
    [ "AppController", "d3/de7/a00039.html#a0e5d93da8106c537df61108964f8afe1", null ],
    [ "~AppController", "d3/de7/a00039.html#a0d778c3e8ba933679c997520c4819032", null ],
    [ "GetPTreeState", "d3/de7/a00039.html#a3e877564de67d85e49d0565c402b88ff", null ],
    [ "GetTimings", "d3/de7/a00039.html#a39410c529bf2eeb7b8fa8236f1d5f521", null ],
    [ "Log", "d3/de7/a00039.html#ab3b2cae1364825bed620318fb8af6158", null ],
    [ "SetPTreeState", "d3/de7/a00039.html#a3827591938c59c51aa622206e58f607a", null ],
    [ "closeEvent", "d3/de7/a00039.html#ad66daf9d8f95d96d53797980161e394d", null ],
    [ "SLOT_AboutDialog", "d3/de7/a00039.html#a1ef67795c0e18bf417839a0972f63dee", null ],
    [ "SLOT_WorkspaceWizard", "d3/de7/a00039.html#a3e43bf39bd08e8f6ea623dcd77e161ef", null ],
    [ "InitMenu", "d3/de7/a00039.html#a3b6db8343807ccfe65d0baeadfd4faa4", null ],
    [ "InitProject", "d3/de7/a00039.html#a3fc26e4449a3d7034c6b2c8dda1f807a", null ],
    [ "InitWidgets", "d3/de7/a00039.html#a38bb18b71d1587990d81e2b876b5ee30", null ],
    [ "InitWorkspace", "d3/de7/a00039.html#a91d5fd7c909ee6b4e1a05960ace769fe", null ],
    [ "SetWorkspaceWindowTitle", "d3/de7/a00039.html#aac04db0763949665f96e0737eb9bcf0e", null ],
    [ "m_factory", "d3/de7/a00039.html#ad773f0ecc65066b640f6d7556dbfcb5e", null ],
    [ "m_settings", "d3/de7/a00039.html#a5387ca4121d22e99dd4d5f7d17b64abc", null ],
    [ "m_log_dock", "d3/de7/a00039.html#ad7b01143ca1ee8ec504104336f27e537", null ],
    [ "m_workspace_controller", "d3/de7/a00039.html#a2c92a3ddd1ca7decf48c680934ef600a", null ]
];