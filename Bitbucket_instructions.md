Basic Git usage
===============

## Some very basic instructions

*Written by Fouad*

### Instructions:

1. Clone repository vleaf_your_name to your pc
2. Make really good changes
3. Commit and push them to vleaf_your_name
4. Then go to bitbucket.com and press on pull request on vleaf_your_name
    This way you ask the administrator to merge your changes with the blessed directory
5. ~~Knowing that you are one of the administrators you can yourself accept the changes by simply pressing the (green) merge button.~~ Now your great changes are merged with the blessed directory  
*Note by Mathias: One team member at a time will be responsible for merging patches to the blessed repo. You don't merge your own pull requests.*


## Keeping everything in sync

*Written by Mathias*

### Adding blessed remote

Specify a new remote repository that will be synced with the fork. In this case we will add it `blessed`.

    $ git remote add blessed https://MathiasB@bitbucket.org/oakstrong/vleaf.git

If you list the remotes, it will now also show the blessed repo.

    $ git remote -v

e.g.

    blessed	https://MathiasB@bitbucket.org/oakstrong/vleaf.git (fetch)
    blessed	https://MathiasB@bitbucket.org/oakstrong/vleaf.git (push)
    origin	https://MathiasB@bitbucket.org/oakstrong/vleaf_mathiasb.git (fetch)
    origin	https://MathiasB@bitbucket.org/oakstrong/vleaf_mathiasb.git (push)


### Syncing blessed repo with your local copy

Fetch the branches and their respective commits from the blessed repository. Commits to `master` will be stored in a local branch, `blessed/master`.

    $ git fetch blessed

This will yield an output like this:

    remote: Counting objects: 8, done.
    remote: Compressing objects: 100% (8/8), done.
    remote: Total 8 (delta 3), reused 0 (delta 0)
    Unpacking objects: 100% (8/8), done.
    From https://bitbucket.org/oakstrong/vleaf
     * [new branch]      master     -> blessed/master

Check out your fork's local master branch (if needed), and merge the changes from `blessed/master` into your local master branch.

    $ git checkout master
    $ git merge blessed/master

If everything is good, you'll see an output like this:

    Updating ff3fc7e..d789ab1
    Fast-forward
     .gitignore                 |  1 +
     Bitbucket_instructions.txt | 23 +++++++++++++++++++++++
     2 files changed, 24 insertions(+)
     create mode 100644 Bitbucket_instructions.txt

But it isn't alway that easy. We working in multiple files, you can encounter merge conflicts. (Git will give you a nice output for that).  
To fix them, you must open the file with conflicts, and manually look for the git annotations and fix everything. Try your local changes, and commit them.

    $ git add THE_CHANGED_FILES
    $ git merge

The `git merge` command will open the editor in the command line with a default commit message. Just save the file and your commit will be done.

**If you want to avoid merge conflicts and problems while merging with the blessed repo (manually or using pull requests), you must frequently sync your repo with the blessed. You avoid complicated merges by keeping everything synced.**

### Pull requests

If needed, I'll add some more info about working with pull requests.


## Practice:

Try to change this file by putting your name or whatever under 'Names'
and merging with the blessed directory.

Names:

* Fouad
* Mathias
* ...
* ...
* ...
